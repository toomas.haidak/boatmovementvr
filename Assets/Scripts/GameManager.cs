using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private Boatcontrols controls;
    [SerializeField] GameObject autoConfigs;
    [SerializeField] GameObject boatParent;
    // Start is called before the first frame update

    private void Awake() {
        controls = new Boatcontrols();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

   
    }

    public void OpenAutoconfigs() {
        
        if(!autoConfigs.activeSelf) {
            autoConfigs.SetActive(true);
            boatParent.GetComponent<BoatMovement>().enabled = false;
        } else {
            autoConfigs.SetActive(false);
            boatParent.GetComponent<BoatMovement>().enabled = true;
        }
    }

    private void OnEnable() {
        controls.Enable();
    }

    private void OnDisable() {
        controls.Disable();
    }
}
