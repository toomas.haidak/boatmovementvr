using DG.Tweening;
using Ericius;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;


public class BoatMovement : MonoBehaviour {
    // public InputActionProperty trigger;

    GameObject boatShake;
    GameObject boatObject;
    GameObject rightController;
    GameObject leftController;
    GameObject flameStream;
    GameObject flameStreamLeft;
    GameObject waterSplashLeft;
    GameObject waterSplashRight;
    public float turbo;
    public float speed;
    float tiltAngle = 0;
    //float angle;
    //float angleLastFrame;

    private Boatcontrols controls;

    float vignetteToAdd = 0f;
    float extraVignetteToAdd = 0f;
    float vignetteValueX = 0.5f;
    float rotateValueX = 0f;

    Quaternion endOfShakeRotation;


    public Volume volume;
    Vignette vignette;

    // Start is called before the first frame update

    private void Awake() {
        controls = new Boatcontrols();




        //   AutoConfigs.SubscribeSpeed11Changed(OnSpeedChanged, this.gameObject);
    }

    //private void OnSpeedChanged() {
    //    speed = AutoConfigs.GetSpeed11();
    //}

    void Start() {

        boatShake = GameObject.Find("BoatShake");
        boatObject = GameObject.Find("BoatObject");
        rightController = GameObject.Find("RightHand Controller");
        leftController = GameObject.Find("LeftHand Controller");
        flameStream = GameObject.Find("FlameStream");
        flameStreamLeft = GameObject.Find("FlameStreamLeft");
        waterSplashLeft = GameObject.Find("WaterSplashLeft");
        waterSplashRight = GameObject.Find("WaterSplashRight");

        volume = GameObject.Find("Volume").GetComponent<Volume>();
        Vignette temp;

        if (volume.profile.TryGet(out temp)) {
            vignette = temp;
        }



        // trigger.action.Enable();
        //trigger.action.performed += OnMovementChange;
    }

    // Update is called once per frame
    void FixedUpdate() {
        speed = AutoConfigs.GetSpeed();

        if (CheckIfControllerInFront() && CheckIfRightControllerAlignedWithHMD() && CheckIfLeftControllerAlignedWithHMD()) {
            speed = speed * 2;

        }

        // Moving with right controller trigger pressed
        if (controls.Boatmap.Moveboat.ReadValue<float>() > 0 &&
            controls.Boatmap.MoveboatLeft.ReadValue<float>() <= 0) {

            Quaternion current = transform.localRotation;
            RightControllerTurn();

            Moveforward(1f);
            Tilt(current, speed, 1f);
            // TiltWithRightController();
            ShakeBoat();

            flameStream.SetActive(true);
            flameStreamLeft.SetActive(false);
            extraVignetteToAdd = 0f;
            vignette.intensity.value = 0.5f;

            AddVignette(current, 0f);

            if (rotateValueX < 0f) {
                LowerNoseDown();
            }

            waterSplashLeft.SetActive(false);
            waterSplashRight.SetActive(false);
        }

        //Moving with left controller trigger pressed
        if (controls.Boatmap.MoveboatLeft.ReadValue<float>() > 0 &&
            controls.Boatmap.Moveboat.ReadValue<float>() <= 0) {

            Quaternion current = transform.localRotation;
            LeftControllerTurn();

            Moveforward(1f);
            Tilt(current, speed, 1f);
            ShakeBoat();

            flameStreamLeft.SetActive(true);
            flameStream.SetActive(false);
            extraVignetteToAdd = 0f;
            vignette.intensity.value = 0.5f;

            AddVignette(current, 0f);

            if (rotateValueX < 0f) {
                LowerNoseDown();
            }

            waterSplashLeft.SetActive(false);
            waterSplashRight.SetActive(false);
        }

        // Turbo - both trigger pressed
        if (controls.Boatmap.MoveboatLeft.ReadValue<float>() > 0 && controls.Boatmap.Moveboat.ReadValue<float>() > 0) {
            Quaternion current = transform.localRotation;
            RightControllerTurn();
            LeftControllerTurn();

            turbo = AutoConfigs.GetTurbo();
            Moveforward(turbo);
            Tilt(current, speed, turbo);
            ShakeBoat();


            flameStream.SetActive(true);
            flameStreamLeft.SetActive(true);

            waterSplashLeft.SetActive(true);
            waterSplashRight.SetActive(true);

            AddVignette(current, 0.01f);

            if (rotateValueX > -5f) {
                RaiseNoseUp();
            }
        }

        if (controls.Boatmap.MoveboatLeft.ReadValue<float>() <= 0 && controls.Boatmap.Moveboat.ReadValue<float>() <= 0) {
            ResetValues();

            if (rotateValueX < 0f) {
                LowerNoseDown();
            }

            TiltCenter();

            CenterBoatShake();
        }

    }

    //public void OnMovementChange(InputAction.CallbackContext context) {

    //    transform.position += Vector3.forward * Time.deltaTime;

    //}

    void RightControllerTurn() {
        Quaternion current = transform.localRotation;
        Quaternion rotation = rightController.transform.rotation;
        // Quaternion finalRotation = initialRotation * Quaternion.Euler(0, 180f, 0);

        transform.localRotation = Quaternion.Slerp(current, rotation, Time.fixedDeltaTime);
        transform.localRotation = Quaternion.Euler(new Vector3(0f, transform.localRotation.eulerAngles.y, 0f));
    }

    void LeftControllerTurn() {
        Quaternion current = transform.localRotation;
        Quaternion rotation = leftController.transform.rotation;
        // Quaternion finalRotation = initialRotation * Quaternion.Euler(0, 180f, 0);

        transform.localRotation = Quaternion.Slerp(current, rotation, Time.fixedDeltaTime);
        transform.localRotation = Quaternion.Euler(new Vector3(0f, transform.localRotation.eulerAngles.y, 0f));
    }

    void AddVignette(Quaternion current, float extraIntensity) {
        if (transform.localRotation.eulerAngles.y > current.eulerAngles.y + 0.4) {
            vignetteToAdd += 0.01f;
            extraVignetteToAdd = Mathf.Clamp((extraVignetteToAdd + extraIntensity), 0f, 0.15f);
            vignette.intensity.value = 0.5f + extraVignetteToAdd;
            vignette.center.value = new Vector2(Mathf.Clamp((0.5f - vignetteToAdd), 0.3f, 0.5f), 0.5f);
            vignetteValueX = Mathf.Clamp((0.5f - vignetteToAdd), 0.3f, 0.5f);

        } else if (transform.localRotation.eulerAngles.y < current.eulerAngles.y - 0.4) {
            vignetteToAdd += 0.01f;
            extraVignetteToAdd = Mathf.Clamp((extraVignetteToAdd + extraIntensity), 0f, 0.15f);
            vignette.intensity.value = 0.5f + extraVignetteToAdd;
            vignette.center.value = new Vector2(Mathf.Clamp((0.5f + vignetteToAdd), 0.5f, 0.7f), 0.5f);
            vignetteValueX = Mathf.Clamp((0.5f + vignetteToAdd), 0.5f, 0.7f);
        } else {
            vignetteToAdd = 0f;
            extraVignetteToAdd = Mathf.Clamp((extraVignetteToAdd + extraIntensity), 0f, 0.3f);

            if (vignetteValueX < 0.5f) {
                vignette.center.value = new Vector2((vignetteValueX + 0.01f), 0.5f);
                vignetteValueX += 0.01f;
                vignette.intensity.value = (vignette.intensity.value - 0.01f) + extraVignetteToAdd;
            }

            if (vignetteValueX > 0.5f) {
                vignette.center.value = new Vector2((vignetteValueX - 0.01f), 0.5f);
                vignetteValueX -= 0.01f;
                vignette.intensity.value = (vignette.intensity.value - 0.01f) + extraVignetteToAdd;
            }

            if (vignetteValueX == 0.5f) {
                vignette.intensity.value = Mathf.Clamp((vignette.intensity.value + extraIntensity), 0f, 0.8f);

            }
        }
    }

    void RaiseNoseUp() {
        boatObject.transform.Rotate(-0.1f, 0, 0f);
        rotateValueX -= 0.1f;
    }

    void LowerNoseDown() {
        boatObject.transform.Rotate(0.3f, 0, 0f);
        rotateValueX += 0.3f;
    }

    void Tilt(Quaternion current, float speed, float turbo) {
        if (transform.localRotation.eulerAngles.y > current.eulerAngles.y + 0.4) {
            if (tiltAngle > -speed * turbo * 10f && tiltAngle > -15f) {
                boatObject.transform.Rotate(0f, 0f, -speed * turbo * 0.2f);
                tiltAngle = Mathf.Clamp((tiltAngle - speed * turbo * 0.2f), -speed * turbo * 10f, 15f);
            } else {
                if (tiltAngle < -speed * turbo * 10f) {
                    boatObject.transform.Rotate(0f, 0f, speed * turbo * 0.2f);
                    tiltAngle = Mathf.Clamp((tiltAngle + speed * turbo * 0.2f), -15f, -speed * turbo * 10f);
                }
            }
        } else {

            if (transform.localRotation.eulerAngles.y < current.eulerAngles.y - 0.4) {
                if (tiltAngle < speed * turbo * 10f && tiltAngle < 15f) {
                    boatObject.transform.Rotate(0f, 0f, speed * turbo * 0.2f);
                    tiltAngle = Mathf.Clamp((tiltAngle + speed * turbo * 0.2f), -15f, speed * turbo * 10f);
                } else {
                    if (tiltAngle > speed * turbo * 10f) {
                        boatObject.transform.Rotate(0f, 0f, speed * turbo * -0.2f);
                        tiltAngle = Mathf.Clamp((tiltAngle - speed * turbo * 0.2f), speed * turbo * 10f, 15f);
                    }
                }
            } else {
                TiltCenter();
            }
        }
    }

    void TiltCenter() {
        if (tiltAngle < 0) {
            boatObject.transform.Rotate(0f, 0f, 0.5f);
            tiltAngle = Mathf.Clamp((tiltAngle += 0.5f), -20f, 0);
        } else {
            if (tiltAngle > 0) {
                boatObject.transform.Rotate(0f, 0f, -0.5f);
                tiltAngle = Mathf.Clamp((tiltAngle -= 0.5f), 0, 20f);
            }
        }
    }

    bool CheckIfControllerInFront() {
        GameObject camera = GameObject.Find("Main Camera");
        Vector3 cameraPosition = GameObject.Find("Main Camera").transform.position;
        RaycastHit hit;
        if (Physics.SphereCast(cameraPosition, 0.2f, camera.transform.forward, out hit, 1f)) {

            if (hit.collider.name == "Hairdryer") {
                return true;
            }
        }

        return false;
    }

    bool CheckIfRightControllerAlignedWithHMD() {
        GameObject camera = GameObject.Find("Main Camera");
        GameObject controllerRight = GameObject.Find("RightHand Controller");
        if (camera.transform.eulerAngles.y < controllerRight.transform.eulerAngles.y + 20 &&
            camera.transform.eulerAngles.y > controllerRight.transform.eulerAngles.y - 20) {
            return true;
        }
        return false;
    }

    bool CheckIfLeftControllerAlignedWithHMD() {
        GameObject camera = GameObject.Find("Main Camera");
        GameObject controllerRight = GameObject.Find("LeftHand Controller");
        if (camera.transform.eulerAngles.y < controllerRight.transform.eulerAngles.y + 20 &&
            camera.transform.eulerAngles.y > controllerRight.transform.eulerAngles.y - 20) {
            return true;
        }
        return false;
    }

    //void TiltWithRightController() {
    //    angle = rightController.transform.localRotation.eulerAngles.y;
    //    if (angle > angleLastFrame + 1) {
    //        boatObject.transform.Rotate(0f, 0f, -speed * turbo * 0.5f);

    //    } else {
    //        if (angle < angleLastFrame - 1) {
    //            boatObject.transform.Rotate(0f, 0f, speed * turbo * 0.5f);
    //        }
    //    }
    //    angleLastFrame = angle;
    //}

    void Moveforward(float turbo) {
        transform.Translate(0, 0, 3 * speed * turbo * Time.deltaTime);
    }

    void ShakeBoat() {
        boatShake.transform.DOShakeRotation(1f, new Vector3(1f, 0.1f, 0.1f), 1, 90f, false);
    }

    void CenterBoatShake() {
        if (boatShake.transform.localRotation.eulerAngles.x < 359.9f &&
            boatShake.transform.localRotation.eulerAngles.x > 355f) {
            boatShake.transform.Rotate(0.1f, 0f, 0f);
        }

        if (boatShake.transform.localRotation.eulerAngles.x > 0.1f &&
            boatShake.transform.localRotation.eulerAngles.x < 5) {
            boatShake.transform.Rotate(-0.1f, 0f, 0f);
        }

        if (boatShake.transform.localRotation.eulerAngles.y < 359.9f &&
            boatShake.transform.localRotation.eulerAngles.y > 355f) {
            boatShake.transform.Rotate(0f, 0.1f, 0f);
        }

        if (boatShake.transform.localRotation.eulerAngles.y > 0.1f &&
            boatShake.transform.localRotation.eulerAngles.y < 5) {
            boatShake.transform.Rotate(0f, -0.1f, 0f);
        }

        if (boatShake.transform.localRotation.eulerAngles.y < 359.9f &&
            boatShake.transform.localRotation.eulerAngles.y > 355f) {
            boatShake.transform.Rotate(0f, 0f, 0.1f);
        }

        if (boatShake.transform.localRotation.eulerAngles.z > 0.1f &&
            boatShake.transform.localRotation.eulerAngles.z < 5) {
            boatShake.transform.Rotate(0f, 0f, -0.1f);
        }
    }


    void ResetValues() {
        flameStream.SetActive(false);
        flameStreamLeft.SetActive(false);
        vignette.intensity.value = 0f;
        vignette.center.value = new Vector2(0.5f, 0.5f);
        vignetteToAdd = 0f;
        vignetteValueX = 0.5f;
        extraVignetteToAdd = 0f;
        waterSplashLeft.SetActive(false);
        waterSplashRight.SetActive(false);
        // boatShake.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);



    }

    // in update
    // Vector3 realtivePos = target.position - transform.position;
    // transform.rotation = Quaternion.LookRotation(relativePos, new Vector 3 (0,1,0));

    public void Rotate() {
        Vector3 rotationToAdd = new Vector3(0, 1, 0);
        transform.Rotate(rotationToAdd);
    }

    private void OnEnable() {
        controls.Enable();
    }

    private void OnDisable() {
        controls.Disable();
    }
}
