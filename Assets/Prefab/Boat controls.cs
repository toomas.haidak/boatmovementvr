// GENERATED AUTOMATICALLY FROM 'Assets/Prefab/Boat controls.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Boatcontrols : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Boatcontrols()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Boat controls"",
    ""maps"": [
        {
            ""name"": ""Boatmap"",
            ""id"": ""2548ea54-2362-41b7-9c3d-725615f0e680"",
            ""actions"": [
                {
                    ""name"": ""Moveboat"",
                    ""type"": ""Value"",
                    ""id"": ""4dc06cea-491e-418f-a0c7-fbd8d5e984b5"",
                    ""expectedControlType"": ""Analog"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""MoveboatLeft"",
                    ""type"": ""Value"",
                    ""id"": ""86816742-a26b-496f-808c-ef669ac2a3c1"",
                    ""expectedControlType"": ""Analog"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""6e501eb5-05da-4790-8644-da8ff343649e"",
                    ""path"": ""<XRController>{RightHand}/triggerPressed"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moveboat"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""0064e83c-6481-48ec-a1d1-94b7cab78907"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moveboat"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""698b64d1-2607-48fe-939d-c9ee07151087"",
                    ""path"": ""<Keyboard>/upArrow"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Moveboat"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""b1a0399c-c39a-40b7-b1fa-3bdd62c1034d"",
                    ""path"": ""<XRController>{LeftHand}/triggerPressed"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""MoveboatLeft"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        },
        {
            ""name"": ""UI"",
            ""id"": ""5aa2555f-5d30-4529-b3e5-34b2e365f2ff"",
            ""actions"": [
                {
                    ""name"": ""OpenUI"",
                    ""type"": ""Button"",
                    ""id"": ""9500b2ca-6776-495a-8d49-619cf1ecb99a"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""a78787ac-6e1d-4aaf-97cc-986881be27ed"",
                    ""path"": ""<XRController>{RightHand}/secondaryButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""OpenUI"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Boatmap
        m_Boatmap = asset.FindActionMap("Boatmap", throwIfNotFound: true);
        m_Boatmap_Moveboat = m_Boatmap.FindAction("Moveboat", throwIfNotFound: true);
        m_Boatmap_MoveboatLeft = m_Boatmap.FindAction("MoveboatLeft", throwIfNotFound: true);
        // UI
        m_UI = asset.FindActionMap("UI", throwIfNotFound: true);
        m_UI_OpenUI = m_UI.FindAction("OpenUI", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Boatmap
    private readonly InputActionMap m_Boatmap;
    private IBoatmapActions m_BoatmapActionsCallbackInterface;
    private readonly InputAction m_Boatmap_Moveboat;
    private readonly InputAction m_Boatmap_MoveboatLeft;
    public struct BoatmapActions
    {
        private @Boatcontrols m_Wrapper;
        public BoatmapActions(@Boatcontrols wrapper) { m_Wrapper = wrapper; }
        public InputAction @Moveboat => m_Wrapper.m_Boatmap_Moveboat;
        public InputAction @MoveboatLeft => m_Wrapper.m_Boatmap_MoveboatLeft;
        public InputActionMap Get() { return m_Wrapper.m_Boatmap; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(BoatmapActions set) { return set.Get(); }
        public void SetCallbacks(IBoatmapActions instance)
        {
            if (m_Wrapper.m_BoatmapActionsCallbackInterface != null)
            {
                @Moveboat.started -= m_Wrapper.m_BoatmapActionsCallbackInterface.OnMoveboat;
                @Moveboat.performed -= m_Wrapper.m_BoatmapActionsCallbackInterface.OnMoveboat;
                @Moveboat.canceled -= m_Wrapper.m_BoatmapActionsCallbackInterface.OnMoveboat;
                @MoveboatLeft.started -= m_Wrapper.m_BoatmapActionsCallbackInterface.OnMoveboatLeft;
                @MoveboatLeft.performed -= m_Wrapper.m_BoatmapActionsCallbackInterface.OnMoveboatLeft;
                @MoveboatLeft.canceled -= m_Wrapper.m_BoatmapActionsCallbackInterface.OnMoveboatLeft;
            }
            m_Wrapper.m_BoatmapActionsCallbackInterface = instance;
            if (instance != null)
            {
                @Moveboat.started += instance.OnMoveboat;
                @Moveboat.performed += instance.OnMoveboat;
                @Moveboat.canceled += instance.OnMoveboat;
                @MoveboatLeft.started += instance.OnMoveboatLeft;
                @MoveboatLeft.performed += instance.OnMoveboatLeft;
                @MoveboatLeft.canceled += instance.OnMoveboatLeft;
            }
        }
    }
    public BoatmapActions @Boatmap => new BoatmapActions(this);

    // UI
    private readonly InputActionMap m_UI;
    private IUIActions m_UIActionsCallbackInterface;
    private readonly InputAction m_UI_OpenUI;
    public struct UIActions
    {
        private @Boatcontrols m_Wrapper;
        public UIActions(@Boatcontrols wrapper) { m_Wrapper = wrapper; }
        public InputAction @OpenUI => m_Wrapper.m_UI_OpenUI;
        public InputActionMap Get() { return m_Wrapper.m_UI; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(UIActions set) { return set.Get(); }
        public void SetCallbacks(IUIActions instance)
        {
            if (m_Wrapper.m_UIActionsCallbackInterface != null)
            {
                @OpenUI.started -= m_Wrapper.m_UIActionsCallbackInterface.OnOpenUI;
                @OpenUI.performed -= m_Wrapper.m_UIActionsCallbackInterface.OnOpenUI;
                @OpenUI.canceled -= m_Wrapper.m_UIActionsCallbackInterface.OnOpenUI;
            }
            m_Wrapper.m_UIActionsCallbackInterface = instance;
            if (instance != null)
            {
                @OpenUI.started += instance.OnOpenUI;
                @OpenUI.performed += instance.OnOpenUI;
                @OpenUI.canceled += instance.OnOpenUI;
            }
        }
    }
    public UIActions @UI => new UIActions(this);
    public interface IBoatmapActions
    {
        void OnMoveboat(InputAction.CallbackContext context);
        void OnMoveboatLeft(InputAction.CallbackContext context);
    }
    public interface IUIActions
    {
        void OnOpenUI(InputAction.CallbackContext context);
    }
}
