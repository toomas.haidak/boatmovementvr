﻿using UnityEngine;

namespace Ericius.UnityRemoteData.Samples.Test
{
    public class ManualTestGetAndSetRemoteData : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.G))
            {
                Debug.Log("Send get remote data request");
                UnityRemoteData.Get(
                    "Test",
                    data => { Debug.Log("Get remote data success: " + JsonUtility.FromJson<TestData>(data)); },
                    error => { Debug.Log("Get remote data error: " + error); }
                );
            }

            if (Input.GetKeyDown(KeyCode.S))
            {
                Debug.Log("Send set remote data request");
                UnityRemoteData.Set(
                    "Test",
                    JsonUtility.ToJson(new TestData()),
                    success => { Debug.Log("Set remote data success=" + success); }
                );
            }
        }

        private class TestData
        {
            public string TestString = "Lorem Ipsum";
            public int TestInt = 42;
            public float TestFloat = 0.92f;
            public bool TestBool = true;

            public override string ToString()
            {
                return
                    $"{nameof(TestString)}: {TestString}, {nameof(TestInt)}: {TestInt}, {nameof(TestFloat)}: {TestFloat}, {nameof(TestBool)}: {TestBool}";
            }
        }
    }
}