﻿using System;
using Ericius.DotNetRemoteData;
using Ericius.DotNetRemoteData.Serialize;
using Ericius.UnityRemoteData.DataProviders;
using Ericius.UnityRemoteData.Utils;
using UnityEngine;

namespace Ericius.UnityRemoteData
{
    public class UnityRemoteData : SingletonMonoBehaviour<UnityRemoteData>
    {
        [SerializeField] private AbstractUnityRemoteDataProviderFactory DataProviderFactory;

        private RemoteData RemoteDataInstance { get; set; }

        protected override void Initialize()
        {
            RemoteDataInstance = new RemoteData(Application.productName, DataProviderFactory.Get());
            RemoteDataInstance.AddSerializer(new ZipStringSerializer());
        }

        public static void Get(string key, Action<string> onSuccess, Action<string> onError)
        {
            Instance.RemoteDataInstance.Get(key, onSuccess, onError);
        }

        public static void Set(string key, string value, Action<bool> onComplete)
        {
            Instance.RemoteDataInstance.Set(key, value, onComplete);
        }
    }
}