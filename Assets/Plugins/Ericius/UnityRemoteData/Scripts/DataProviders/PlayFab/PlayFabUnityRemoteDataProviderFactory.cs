﻿using Ericius.DotNetRemoteData.DataProviders;
using Ericius.DotNetRemoteData.DataProviders.PlayFabRemoteDataProvider;
using UnityEngine;

namespace Ericius.UnityRemoteData.DataProviders.PlayFab
{
    public class PlayFabUnityRemoteDataProviderFactory : AbstractUnityRemoteDataProviderFactory
    {
        [SerializeField] private string PlayFabTitleId = "B9886";

        public override IRemoteDataProvider Get()
        {
            return new PlayFabRemoteDataProvider(PlayFabTitleId, SystemInfo.deviceUniqueIdentifier);
        }
    }
}