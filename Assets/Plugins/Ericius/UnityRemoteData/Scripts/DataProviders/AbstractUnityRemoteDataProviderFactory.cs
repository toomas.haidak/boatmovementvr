﻿using Ericius.DotNetRemoteData.DataProviders;
using UnityEngine;

namespace Ericius.UnityRemoteData.DataProviders
{
    public abstract class AbstractUnityRemoteDataProviderFactory : MonoBehaviour
    {
        public abstract IRemoteDataProvider Get();
    }
}