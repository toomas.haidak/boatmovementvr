﻿*About
Sync any string data with cloud save.

*Integration*
Add UnityRemoteData prefab to scene.

*API*
RemoteData.Get(...)
RemoteData.Set(...)