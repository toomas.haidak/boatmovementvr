﻿using System;
using Ericius.DotNetRemoteData.DataProviders.ChunksData;
using PlayFab;
using PlayFab.ClientModels;
using PlayFab.Json;

namespace Ericius.DotNetRemoteData.DataProviders.PlayFabRemoteDataProvider
{
    public class PlayFabRemoteDataProvider : IRemoteDataProvider
    {
        private readonly string _playFabTitleId;
        private readonly string _playFabPlayerId;

        private const int MaxDataChunkSizeInBytes = 10000;

        private readonly ChunksRemoteDataHelper _chunksRemoteDataHelper;

        public PlayFabRemoteDataProvider(string playFabTitleId, string playFabPlayerId)
        {
            _playFabTitleId = playFabTitleId;
            _playFabPlayerId = playFabPlayerId;
            _chunksRemoteDataHelper = new ChunksRemoteDataHelper(
                MaxDataChunkSizeInBytes, PullDataFromPlayFab, PushDataToPlayFab
            );
        }

        public void Pull(PullRemoteDataRequest request, Action<PullRemoteDataResponse> onComplete)
        {
            LoginToPlayFab(success =>
            {
                if (success)
                {
                    _chunksRemoteDataHelper.PullAsChunks(request, onComplete);
                }
                else
                {
                    onComplete.Invoke(
                        PullRemoteDataResponse.GenerateFromRequest(request, false, "Can't login to PlayFab")
                    );
                }
            });
        }

        public void Push(PushRemoteDataRequest request, Action<PushRemoteDataResponse> onComplete)
        {
            LoginToPlayFab(loginSuccess =>
            {
                if (loginSuccess)
                {
                    _chunksRemoteDataHelper.PushAsChunks(request, onComplete);
                }
                else
                {
                    onComplete.Invoke(
                        PushRemoteDataResponse.GenerateFromRequest(request, false, "Can't login to PlayFab")
                    );
                }
            });
        }

        private void PullDataFromPlayFab(PullRemoteDataRequest request,
            Action<PullRemoteDataResponse> onComplete)
        {
            ExecuteCloudScriptRequest pullConfigsRequest = new ExecuteCloudScriptRequest
            {
                FunctionName = "getData",
                FunctionParameter = PlayFabSimpleJson.SerializeObject(request),
                GeneratePlayStreamEvent = true
            };

            PlayFabClientAPI.ExecuteCloudScript(pullConfigsRequest,
                result =>
                {
                    if (result.FunctionResult != null)
                    {
                        PullRemoteDataResponse responseData =
                            PlayFabSimpleJson.DeserializeObject<PullRemoteDataResponse>(
                                result.FunctionResult.ToString()
                            );
                        responseData.Success = true;
                        onComplete.Invoke(responseData);
                    }
                    else
                    {
                        onComplete.Invoke(
                            PullRemoteDataResponse.GenerateFromRequest(request, false, "Null result")
                        );
                    }
                }, error =>
                {
                    OnPlayfabError(error);
                    onComplete.Invoke(
                        PullRemoteDataResponse.GenerateFromRequest(
                            request, false, "PlayFab error => " + error.ErrorMessage
                        )
                    );
                });
        }

        private void PushDataToPlayFab(PushRemoteDataRequest request, Action<PushRemoteDataResponse> onComplete)
        {
            ExecuteCloudScriptRequest pushConfigsRequest = new ExecuteCloudScriptRequest
            {
                FunctionName = "setData",
                FunctionParameter = PlayFabSimpleJson.SerializeObject(request),
                GeneratePlayStreamEvent = true
            };
            PlayFabClientAPI.ExecuteCloudScript(pushConfigsRequest, result =>
            {
                onComplete.Invoke(
                    PushRemoteDataResponse.GenerateFromRequest(request, true)
                );
            }, error =>
            {
                OnPlayfabError(error);
                onComplete.Invoke(
                    PushRemoteDataResponse.GenerateFromRequest(request, false, error.ErrorMessage)
                );
            });
        }

        private void LoginToPlayFab(Action<bool> onComplete)
        {
            if (PlayFabClientAPI.IsClientLoggedIn())
            {
                Console.WriteLine("Already logged in to PlayFab");
                onComplete.Invoke(true);
                return;
            }

            PlayFabSettings.TitleId = _playFabTitleId;

            LoginWithCustomIDRequest request = new LoginWithCustomIDRequest
            {
                CustomId = _playFabPlayerId,
                CreateAccount = true
            };
            PlayFabClientAPI.LoginWithCustomID(request, result =>
            {
                Console.WriteLine("PlayFab login success");
                onComplete.Invoke(true);
            }, error =>
            {
                OnPlayfabError(error);
                onComplete.Invoke(false);
            });
        }

        private void OnPlayfabError(PlayFabError error)
        {
            Console.Error.WriteLine(error.GenerateErrorReport());
        }
    }
}