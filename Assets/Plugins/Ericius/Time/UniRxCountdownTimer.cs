﻿using System;
using System.Collections.Generic;
using UniRx;

namespace Plugins.Ericius.Time
{
    public class UniRxCountdownTimer
    {
        private float _timer;
        private float _totalCountdownTime;

        public FloatReactiveProperty Progress01 { get; private set; } = new FloatReactiveProperty();
        public ReactiveCommand OnCompleted { get; } = new ReactiveCommand();
        public BoolReactiveProperty IsActive { get; private set; } = new BoolReactiveProperty();

        private readonly List<IDisposable> _onCompletedDisposable = new List<IDisposable>();

        public void Run(float time)
        {
            _totalCountdownTime = time;
            _timer = time;

            if (time > 0)
            {
                IsActive.Value = true;
            }
            else
            {
                SetIsCompleted();
            }
        }

        public IDisposable SubscribeOnCompleted(Action onCompleted)
        {
            IDisposable disposable = OnCompleted.Subscribe(unit => onCompleted?.Invoke());
            _onCompletedDisposable.Add(disposable);
            return disposable;
        }

        public void CancelAllOnCompleted()
        {
            for (int i = 0, iSize = _onCompletedDisposable.Count; i < iSize; i++)
            {
                _onCompletedDisposable[i].Dispose();
            }

            _onCompletedDisposable.Clear();
        }

        public void Stop()
        {
            IsActive.Value = false;
            Progress01.Value = 0f;
        }

        private void SetIsCompleted()
        {
            IsActive.Value = false;
            Progress01.Value = 1f;
            OnCompleted.Execute();
        }

        public void Update(float dt)
        {
            if (!IsActive.Value)
            {
                return;
            }

            _timer -= dt;
            if (_timer < 0f)
            {
                _timer = 0f;
            }

            Progress01.Value = (_totalCountdownTime - _timer) / _totalCountdownTime;
            if (Progress01.Value >= 1f)
            {
                SetIsCompleted();
            }
        }

        public IDisposable SubscribeToProgressChanges(Action<float> onNext, bool skipCurrentValue = true)
        {
            if (skipCurrentValue)
            {
                return Progress01.Skip(1).Subscribe(onNext);
            }
            else
            {
                return Progress01.Subscribe(onNext);
            }
        }
    }
}