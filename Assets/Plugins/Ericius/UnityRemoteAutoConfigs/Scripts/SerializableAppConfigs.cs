﻿using System;
using System.Collections.Generic;
using Ericius.AutoConfig.Scripts.Core;

[Serializable]
public class SerializableAppConfigs
{
    public List<SerializableAppConfig> Configs = new List<SerializableAppConfig>();

    public void AddConfig(AutoConfigEntryData entryData)
    {
        Configs.Add(ConvertConfigAttribute(entryData));
    }

    private SerializableAppConfig ConvertConfigAttribute(AutoConfigEntryData entryData)
    {
        return new SerializableAppConfig
        {
            Key = entryData.Key,
            Group = entryData.Group,
            Value = entryData.Value.ToString()
        };
    }
}

[Serializable]
public class SerializableAppConfig
{
    public string Key;
    public string Group;

    public string Value;
}