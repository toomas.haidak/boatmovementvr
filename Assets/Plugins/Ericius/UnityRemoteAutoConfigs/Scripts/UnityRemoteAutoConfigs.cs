﻿using System;
using System.Collections.Generic;
using Ericius.AutoConfig.Scripts.Core;
using Ericius.UnityRemoteData.Utils;
using UnityEngine;
using UnityEngine.UI;

namespace Ericius.RemoteData
{
    public class UnityRemoteAutoConfigs : SingletonMonoBehaviour<UnityRemoteAutoConfigs>
    {
        private const string DataKey = "AutoConfigs";

        [SerializeField] private Button PullConfigsButton;
        [SerializeField] private Button PushConfigsButton;

        private AutoConfigsController _autoConfigsController;

        public override void Awake()
        {
            base.Awake();
            CreateInstance();
        }

        protected override void Initialize()
        {
            base.Initialize();

            _autoConfigsController = FindObjectOfType<AutoConfigsController>();
            if (_autoConfigsController == null)
            {
                Debug.LogWarning("AutoConfigsController not found! Interrupt loading UnityRemoteAutoConfigs");
                return;
            }

            AddRemoteConfigTools();
        }

        private void AddRemoteConfigTools()
        {
            PullConfigsButton.onClick.AddListener(() =>
            {
                _autoConfigsController.AutoConfigsModalPopup.Show("Are you sure want to pull AutoConfigs from cloud?",
                    () =>
                    {
                        _autoConfigsController.AutoConfigsWaitPopup.Show();
                        PullConfigs((success) =>
                        {
                            _autoConfigsController.AutoConfigsWaitPopup.Hide();
                            if (success) _autoConfigsController.AutoConfigsWindowController.Refresh();
                            string pullResultMessage =
                                success ? "Pull AutoConfigs: SUCCESS" : "Pull AutoConfigs: FAILED";
                            _autoConfigsController.AutoConfigsModalPopup.Show(pullResultMessage, () => { }, null);
                        });
                    }, () => { });
            });
            _autoConfigsController.AutoConfigsWindowController.AddTool(PullConfigsButton.transform, 1);

            PushConfigsButton.onClick.AddListener(() =>
            {
                _autoConfigsController.AutoConfigsModalPopup.Show("Are you sure want to push AutoConfigs to cloud?",
                    () =>
                    {
                        _autoConfigsController.AutoConfigsWaitPopup.Show();
                        PushConfigs(success =>
                        {
                            Debug.Log("PushConfigs. Success: " + success);
                            _autoConfigsController.AutoConfigsWaitPopup.Hide();
                            string pushResultMessage =
                                success ? "Push AutoConfigs: SUCCESS" : "Pull AutoConfigs: FAILED";
                            _autoConfigsController.AutoConfigsModalPopup.Show(pushResultMessage, () => { }, null);
                        });
                    }, () => { });
            });
            _autoConfigsController.AutoConfigsWindowController.AddTool(PushConfigsButton.transform, 2);
        }

        private void PullConfigs(Action<bool> onComplete)
        {
            UnityRemoteData.UnityRemoteData.Get(DataKey, jsonData =>
            {
                Debug.Log("Pull configs success");
                ApplyConfigs(JsonUtility.FromJson<SerializableAppConfigs>(jsonData));
                onComplete.Invoke(true);
            }, error =>
            {
                Debug.LogWarning("Pull configs failed: " + error);
                onComplete.Invoke(false);
            });
        }

        private void ApplyConfigs(SerializableAppConfigs appConfigs)
        {
            foreach (SerializableAppConfig appConfig in appConfigs.Configs)
            {
                AutoConfigManager.ApplyConfig(appConfig.Key, appConfig.Value);
            }
        }

        private void PushConfigs(Action<bool> onComplete)
        {
            UnityRemoteData.UnityRemoteData.Set(DataKey, JsonUtility.ToJson(GenerateSerializableConfigs()), onComplete);
        }

        private SerializableAppConfigs GenerateSerializableConfigs()
        {
            List<AutoConfigEntryData> configs = AutoConfigManager.GetConfigs();

            SerializableAppConfigs serializableAppConfigs = new SerializableAppConfigs();
            foreach (AutoConfigEntryData autoConfigAttribute in configs)
            {
                serializableAppConfigs.AddConfig(autoConfigAttribute);
            }

            return serializableAppConfigs;
        }
    }
}