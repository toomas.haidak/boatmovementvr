﻿#if UNITY_EDITOR

using System;
using UnityEditor;

namespace Plugins.Ericius.EditorTools.GameView
{
    [InitializeOnLoad]
    public static class EditorMuteAudioToggle
    {
        private static bool _previousState;
        public static bool CurrentState => EditorUtility.audioMasterMute;
        public static event Action<bool> OnStateChanged;

        static EditorMuteAudioToggle()
        {
            _previousState = CurrentState;
            SceneView.duringSceneGui += OnDrawGUI;
        }

        private static void OnDrawGUI(SceneView obj)
        {
            if (_previousState != CurrentState)
            {
                FireStateChangeEvent();
                _previousState = CurrentState;
            }
        }

        private static void FireStateChangeEvent()
        {
            OnStateChanged?.Invoke(CurrentState);
        }
    }
}
#endif