﻿#if UNITY_EDITOR

using System;
using UnityEditor;

namespace Plugins.Ericius.EditorTools.PlayMode
{
    [InitializeOnLoadAttribute]
    public class EditorPlayModeState
    {
        public static event Action OnEnteredPlayMode;
        public static event Action OnEnteredEditMode;
        public static event Action OnExitingEditMode;
        public static event Action OnExitingPlayMode;

        static EditorPlayModeState()
        {
            EditorApplication.playModeStateChanged += OnPlayModeStateChanged;
        }

        private static void OnPlayModeStateChanged(PlayModeStateChange state)
        {
            switch (state)
            {
                case PlayModeStateChange.EnteredPlayMode:
                    OnEnteredPlayMode?.Invoke();
                    break;

                case PlayModeStateChange.EnteredEditMode:
                    OnEnteredEditMode?.Invoke();
                    break;

                case PlayModeStateChange.ExitingEditMode:
                    OnExitingEditMode?.Invoke();
                    break;

                case PlayModeStateChange.ExitingPlayMode:
                    OnExitingPlayMode?.Invoke();
                    break;

                default:
                    throw new ArgumentOutOfRangeException(nameof(state), state, null);
            }
        }
    }
}
#endif