﻿*About
Sync any string data with cloud save.

*Integration*
Initialize RemoteData with RemoteData.Initialize(...) method.

*API*
RemoteData.Get(...)
RemoteData.Set(...)