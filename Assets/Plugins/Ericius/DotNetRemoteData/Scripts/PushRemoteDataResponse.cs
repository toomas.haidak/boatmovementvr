﻿using System;

namespace Ericius.DotNetRemoteData
{
    [Serializable]
    public class PushRemoteDataResponse
    {
        public string AppId;
        public string DataKey;
    
        public bool Success;
        public string ErrorMessage;
        
        public static PushRemoteDataResponse GenerateFromRequest(PushRemoteDataRequest request,
            bool success, string errorMessage = null)
        {
            return new PushRemoteDataResponse
            {
                AppId = request.AppId,
                DataKey = request.DataKey,
                Success = success,
                ErrorMessage = errorMessage
            };
        }
    }
}