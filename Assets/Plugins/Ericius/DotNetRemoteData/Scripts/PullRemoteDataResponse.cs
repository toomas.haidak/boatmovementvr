﻿using System;

namespace Ericius.DotNetRemoteData
{
    [Serializable]
    public class PullRemoteDataResponse
    {
        public string AppId;
        public string DataKey;

        public string Data;
        public long LastUpdateTime;
        public string LastUpdatePlayerId;
        
        public bool Success;
        public string ErrorMessage;
        
        public static PullRemoteDataResponse GenerateFromRequest(PullRemoteDataRequest request,
            bool success, string errorMessage = null)
        {
            return new PullRemoteDataResponse
            {
                AppId = request.AppId,
                DataKey = request.DataKey,
                Success = success,
                ErrorMessage = errorMessage
            };
        }
    }
}