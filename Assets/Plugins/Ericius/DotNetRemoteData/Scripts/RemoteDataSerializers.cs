﻿using System.Collections.Generic;
using Ericius.DotNetRemoteData.Serialize;

namespace Ericius.DotNetRemoteData
{
    public class RemoteDataSerializers : IStringSerializer
    {
        private readonly List<IStringSerializer> _serializers = new List<IStringSerializer>();

        public void AddSerializer(IStringSerializer serializer)
        {
            _serializers.Add(serializer);
        }

        public string Serialize(string row)
        {
            string serialized = row;
            for (int i = 0, iSize = _serializers.Count; i < iSize; i++)
            {
                serialized = _serializers[i].Serialize(serialized);
            }

            return serialized;
        }

        public string Deserialize(string serialized)
        {
            string row = serialized;
            for (int i = 0, iSize = _serializers.Count; i < iSize; i++)
            {
                row = _serializers[i].Deserialize(row);
            }

            return row;
        }
    }
}