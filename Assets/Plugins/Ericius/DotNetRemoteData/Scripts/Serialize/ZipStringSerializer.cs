﻿using System;
using System.IO;
using System.IO.Compression;
using System.Text;

namespace Ericius.DotNetRemoteData.Serialize
{
    public class ZipStringSerializer : IStringSerializer
    {
        public string Serialize(string row)
        {
            return Convert.ToBase64String(Zip(row));
        }

        public string Deserialize(string serialized)
        {
            return Unzip(Convert.FromBase64String(serialized));
        }

        private byte[] Zip(string str)
        {
            byte[] bytes = Encoding.UTF8.GetBytes(str);

            using (MemoryStream msi = new MemoryStream(bytes))
            using (MemoryStream mso = new MemoryStream())
            {
                using (GZipStream gs = new GZipStream(mso, CompressionMode.Compress))
                {
                    CopyStreamTo(msi, gs);
                }

                return mso.ToArray();
            }
        }

        private void CopyStreamTo(Stream src, Stream dest)
        {
            byte[] bytes = new byte[4096];

            int cnt;

            while ((cnt = src.Read(bytes, 0, bytes.Length)) != 0)
            {
                dest.Write(bytes, 0, cnt);
            }
        }

        private string Unzip(byte[] bytes)
        {
            using (MemoryStream msi = new MemoryStream(bytes))
            using (MemoryStream mso = new MemoryStream())
            {
                using (GZipStream gs = new GZipStream(msi, CompressionMode.Decompress))
                {
                    CopyStreamTo(gs, mso);
                }

                return Encoding.UTF8.GetString(mso.ToArray());
            }
        }
    }
}