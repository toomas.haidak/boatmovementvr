﻿namespace Ericius.DotNetRemoteData.Serialize
{
    public interface IStringSerializer
    {
        string Serialize(string row);
        string Deserialize(string serialized);
    }
}