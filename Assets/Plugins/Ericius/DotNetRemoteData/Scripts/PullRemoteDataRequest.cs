﻿using System;

namespace Ericius.DotNetRemoteData
{
    [Serializable]
    public class PullRemoteDataRequest
    {
        public string AppId;
        public string DataKey;
    }
}