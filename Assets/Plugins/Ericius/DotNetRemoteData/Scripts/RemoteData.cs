﻿using System;
using Ericius.DotNetRemoteData.DataProviders;
using Ericius.DotNetRemoteData.Serialize;

namespace Ericius.DotNetRemoteData
{
    public class RemoteData
    {
        private string _applicationId;
        private IRemoteDataProvider _dataProvider;

        private readonly RemoteDataSerializers _dataSerializers = new RemoteDataSerializers();

        public RemoteData(string applicationId, IRemoteDataProvider dataProvider)
        {
            _applicationId = applicationId;
            _dataProvider = dataProvider;
        }

        /// <param name="key"> E.g. 'Levels'</param>
        /// <param name="onSuccess"> Contains data value</param>
        /// <param name="onError">Contain error message</param>
        public void Get(string key, Action<string> onSuccess, Action<string> onError)
        {
            PullRemoteDataRequest request = new PullRemoteDataRequest
            {
                AppId = _applicationId,
                DataKey = key,
            };
            _dataProvider.Pull(
                request, response =>
                {
                    if (response.Success)
                    {
                        onSuccess.Invoke(
                            _dataSerializers.Deserialize(response.Data)
                        );
                    }
                    else
                    {
                        onError.Invoke(response.ErrorMessage);
                    }
                }
            );
        }

        /// <param name="key"> E.g. 'Levels'</param>
        /// <param name="value"></param>
        /// <param name="onComplete">true == success</param>
        public void Set(string key, string value, Action<bool> onComplete)
        {
            PushRemoteDataRequest request = new PushRemoteDataRequest
            {
                AppId = _applicationId,
                DataKey = key,
                Data = _dataSerializers.Serialize(value)
            };
            _dataProvider.Push(
                request, response => { onComplete.Invoke(response.Success); }
            );
        }

        public void AddSerializer(IStringSerializer serializer)
        {
            _dataSerializers.AddSerializer(serializer);
        }
    }
}