﻿namespace  Ericius.DotNetRemoteData.Utils
{
    public static class StringUtils
    {
        public static string[] SplitBySize(this string str, int chunkSize)
        {
            int strLength = str.Length;
            if (strLength == 0)
            {
                return new string[0];
            }
            
            int chunksAmount = (strLength / chunkSize) + 1;
            string[] chunks = new string[chunksAmount];
            for (int i = 0; i < chunksAmount - 1; i++)
            {
                chunks[i] = str.Substring(i * chunkSize, chunkSize);
            }

            chunks[chunksAmount - 1] = str.Substring(
                (chunksAmount - 1) * chunkSize,
                strLength - (chunksAmount - 1) * chunkSize
            );
            return chunks;
        }
    }
}