﻿using System;

namespace Ericius.DotNetRemoteData.DataProviders.ChunksData
{
    public class ChunksRemoteDataHelper
    {
        private readonly ChunksPullRemoteDataHelper _pullRemoteDataHelper;
        private readonly ChunksPushRemoteDataHelper _pushRemoteDataHelper;

        public ChunksRemoteDataHelper(
            int dataChunkSizeInBytes,
            Action<PullRemoteDataRequest, Action<PullRemoteDataResponse>> pullAction,
            Action<PushRemoteDataRequest, Action<PushRemoteDataResponse>> pushAction
        )
        {
            _pullRemoteDataHelper = new ChunksPullRemoteDataHelper(
                pullAction, GenerateDataDescriptorKey, GenerateDataChunkKey
            );
            _pushRemoteDataHelper = new ChunksPushRemoteDataHelper(
                dataChunkSizeInBytes, pushAction, GenerateDataDescriptorKey, GenerateDataChunkKey
            );
        }

        public void PushAsChunks(PushRemoteDataRequest request, Action<PushRemoteDataResponse> onComplete)
        {
            _pushRemoteDataHelper.SplitToChunksAndPush(request, onComplete);
        }
        
        public void PullAsChunks(PullRemoteDataRequest request, Action<PullRemoteDataResponse> onComplete)
        {
            _pullRemoteDataHelper.PullChunksAndJoin(request, onComplete);
        }

        private string GenerateDataDescriptorKey(string dataKey)
        {
            return $"{dataKey}_Descriptor";
        }

        private string GenerateDataChunkKey(string dataKey, int chunkIndex)
        {
            return $"{dataKey}_Chunk{chunkIndex}";
        }
    }
}