﻿using System;
using System.Text;

namespace Ericius.DotNetRemoteData.DataProviders.ChunksData
{
    public class ChunksPullRemoteDataHelper
    {
        private readonly Action<PullRemoteDataRequest, Action<PullRemoteDataResponse>> _pullAction;
        private readonly Func<string, string> _descriptorKeyGenerator;
        private readonly Func<string, int, string> _chunkKeyGenerator;

        public ChunksPullRemoteDataHelper(
            Action<PullRemoteDataRequest, Action<PullRemoteDataResponse>> pullAction,
            Func<string, string> descriptorKeyGenerator, Func<string, int, string> chunkKeyGenerator
        )
        {
            _pullAction = pullAction;
            _descriptorKeyGenerator = descriptorKeyGenerator;
            _chunkKeyGenerator = chunkKeyGenerator;
        }

        /// Flow:
        /// 1. Pull descriptor
        /// 2. Pull and join chunks
        public void PullChunksAndJoin(PullRemoteDataRequest request, Action<PullRemoteDataResponse> onComplete)
        {
            PullDataDescriptor(request, onPullDescriptorComplete =>
            {
                if (onPullDescriptorComplete.Success)
                {
                    ChunksDataDescriptor dataDescriptor = ChunksDataDescriptor.Deserialize(
                        onPullDescriptorComplete.Data
                    );
                    PullAndJoinDataChunks(dataDescriptor.DataChunksAmount, request, onComplete);
                }
                else
                {
                    onComplete.Invoke(onPullDescriptorComplete);
                }
            });
        }

        private void PullAndJoinDataChunks(int chunksAmount, PullRemoteDataRequest originalRequest,
            Action<PullRemoteDataResponse> onComplete)
        {
            if (chunksAmount == 0)
            {
                PullRemoteDataResponse emptyDataResponse = PullRemoteDataResponse.GenerateFromRequest(
                    originalRequest, true
                );
                emptyDataResponse.Data = string.Empty;
                onComplete.Invoke(emptyDataResponse);
                return;
            }

            StringBuilder resultDataBuilder = new StringBuilder();
            AsyncPullDataChunk(0);

            void AsyncPullDataChunk(int chunkIndex)
            {
                if (chunkIndex > chunksAmount - 1)
                {
                    //finish
                    PullRemoteDataResponse joinedChunksDataResponse = PullRemoteDataResponse.GenerateFromRequest(
                        originalRequest, true
                    );
                    joinedChunksDataResponse.Data = resultDataBuilder.ToString();
                    onComplete.Invoke(joinedChunksDataResponse);
                    return;
                }

                PullRemoteDataRequest pullDataChunkRequest = new PullRemoteDataRequest
                {
                    AppId = originalRequest.AppId,
                    DataKey = _chunkKeyGenerator(originalRequest.DataKey, chunkIndex)
                };
                _pullAction(pullDataChunkRequest, pushChunkResponse =>
                {
                    if (pushChunkResponse.Success)
                    {
                        resultDataBuilder.Append(pushChunkResponse.Data);
                        //push next chunk
                        AsyncPullDataChunk(chunkIndex + 1);
                    }
                    else
                    {
                        onComplete.Invoke(
                            PullRemoteDataResponse.GenerateFromRequest(originalRequest, false,
                                $"Pull data chunk #{chunkIndex} error => {pushChunkResponse.ErrorMessage}")
                        );
                    }
                });
            }
        }

        private void PullDataDescriptor(
            PullRemoteDataRequest originalRequest, Action<PullRemoteDataResponse> onComplete
        )
        {
            PullRemoteDataRequest pullDataDescriptorRequest = new PullRemoteDataRequest
            {
                AppId = originalRequest.AppId,
                DataKey = _descriptorKeyGenerator(originalRequest.DataKey)
            };
            _pullAction(pullDataDescriptorRequest, pullDescriptorResponse =>
            {
                onComplete.Invoke(
                    pullDescriptorResponse.Success
                        ? pullDescriptorResponse
                        : PullRemoteDataResponse.GenerateFromRequest(
                            originalRequest, false, "Pull descriptor error => " + pullDescriptorResponse.ErrorMessage
                        )
                );
            });
        }
    }
}