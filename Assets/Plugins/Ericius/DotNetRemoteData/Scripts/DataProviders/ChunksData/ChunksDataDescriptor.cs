﻿using System;

namespace  Ericius.DotNetRemoteData.DataProviders.ChunksData
{
    [Serializable]
    public class ChunksDataDescriptor
    {
        public int DataChunksAmount;

        public static string Serialize(ChunksDataDescriptor chunksDataDescriptor)
        {
            return SimpleJson.SerializeObject(chunksDataDescriptor);
        }

        public static ChunksDataDescriptor Deserialize(string serialized)
        {
            return SimpleJson.DeserializeObject<ChunksDataDescriptor>(serialized);
        }
    }
}