﻿using System;
using System.Collections.Generic;
using Ericius.DotNetRemoteData.Utils;

namespace Ericius.DotNetRemoteData.DataProviders.ChunksData
{
    public class ChunksPushRemoteDataHelper
    {
        private readonly int _dataChunkSizeInBytes;
        private readonly Action<PushRemoteDataRequest, Action<PushRemoteDataResponse>> _pushAction;
        private readonly Func<string, string> _descriptorKeyGenerator;
        private readonly Func<string, int, string> _chunkKeyGenerator;

        public ChunksPushRemoteDataHelper(
            int dataChunkSizeInBytes,
            Action<PushRemoteDataRequest, Action<PushRemoteDataResponse>> pushAction,
            Func<string, string> descriptorKeyGenerator, Func<string, int, string> chunkKeyGenerator
        )
        {
            _dataChunkSizeInBytes = dataChunkSizeInBytes;
            _pushAction = pushAction;
            _descriptorKeyGenerator = descriptorKeyGenerator;
            _chunkKeyGenerator = chunkKeyGenerator;
        }


        /// Flow:
        /// 1. Split to chunks
        /// 2. Push descriptor
        /// 2. Push chunks
        public void SplitToChunksAndPush(PushRemoteDataRequest request, Action<PushRemoteDataResponse> onComplete)
        {
            string[] dataChunks = request.Data.SplitBySize(_dataChunkSizeInBytes);

            PushDataDescriptor(request, dataChunks, onPushDescriptorComplete =>
            {
                if (onPushDescriptorComplete.Success)
                {
                    PushDataChunks(request, onComplete, dataChunks);
                }
                else
                {
                    onComplete.Invoke(onPushDescriptorComplete);
                }
            });
        }

        private void PushDataChunks(PushRemoteDataRequest originalRequest, Action<PushRemoteDataResponse> onComplete,
            IReadOnlyList<string> dataChunks)
        {
            if (dataChunks.Count == 0)
            {
                onComplete.Invoke(
                    PushRemoteDataResponse.GenerateFromRequest(originalRequest, true)
                );
                return;
            }

            AsyncPushDataChunk(0);

            void AsyncPushDataChunk(int chunkIndex)
            {
                if (chunkIndex > dataChunks.Count - 1)
                {
                    //finish
                    onComplete.Invoke(
                        PushRemoteDataResponse.GenerateFromRequest(originalRequest, true)
                    );
                    return;
                }

                string dataToPush = dataChunks[chunkIndex];
                PushRemoteDataRequest pushDataChunkRequest = new PushRemoteDataRequest
                {
                    AppId = originalRequest.AppId,
                    Data = dataToPush,
                    DataKey = _chunkKeyGenerator(originalRequest.DataKey, chunkIndex)
                };
                _pushAction(pushDataChunkRequest, pushChunkResponse =>
                {
                    if (pushChunkResponse.Success)
                    {
                        //push next chunk
                        AsyncPushDataChunk(chunkIndex + 1);
                    }
                    else
                    {
                        onComplete.Invoke(
                            PushRemoteDataResponse.GenerateFromRequest(originalRequest, false,
                                $"Push data chunk #{chunkIndex} error => {pushChunkResponse.ErrorMessage}")
                        );
                    }
                });
            }
        }

        private void PushDataDescriptor(
            PushRemoteDataRequest originalRequest, IReadOnlyCollection<string> dataChunks,
            Action<PushRemoteDataResponse> onComplete
        )
        {
            ChunksDataDescriptor chunksDataDescriptor = new ChunksDataDescriptor
            {
                DataChunksAmount = dataChunks.Count
            };
            PushRemoteDataRequest pushDataDescriptorRequest = new PushRemoteDataRequest
            {
                AppId = originalRequest.AppId,
                Data = ChunksDataDescriptor.Serialize(chunksDataDescriptor),
                DataKey = _descriptorKeyGenerator(originalRequest.DataKey)
            };
            _pushAction(pushDataDescriptorRequest, pushDescriptorResponse =>
            {
                onComplete.Invoke(
                    pushDescriptorResponse.Success
                        ? PushRemoteDataResponse.GenerateFromRequest(originalRequest, true)
                        : PushRemoteDataResponse.GenerateFromRequest(
                            originalRequest, false, "Push descriptor error => " + pushDescriptorResponse.ErrorMessage
                        )
                );
            });
        }
    }
}