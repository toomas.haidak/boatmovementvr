﻿using System;

namespace Ericius.DotNetRemoteData.DataProviders
{
    public interface IRemoteDataProvider
    {
        void Pull(PullRemoteDataRequest request, Action<PullRemoteDataResponse> onComplete);
        void Push(PushRemoteDataRequest request, Action<PushRemoteDataResponse> onComplete);
    }
}