﻿using System;

namespace Ericius.DotNetRemoteData
{
    [Serializable]
    public class PushRemoteDataRequest
    {
        public string AppId;
        public string DataKey;
    
        public string Data;
    }
}