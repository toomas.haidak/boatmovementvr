﻿*About
Manage any predefined key-value configs inside application.

*Integration*
Add AutoConfigs prefab to scene root.

*Usage*
Predefine configs on AutoConfigs class.
Edit config values visually with AutoConfigWindow.