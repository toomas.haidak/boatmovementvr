﻿using System;
using System.Globalization;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;

namespace LetC
{
    public class TextKeyboard : MonoBehaviour
    {
        public static TextKeyboard instance;

        [ReadOnly] public TMP_InputField ActiveInputField;

        public Action OnClickDoneKey;

        public bool IsActive
        {
            set => gameObject.SetActive(value);
        }

        public string NumberDecimalSeparator => CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;

        private Vector3 _initialLocalPos;
        
        private void Awake()
        {
            if (instance != null)
            {
                Destroy(gameObject);
            }
            else
            {
                instance = this;
                OnClickDoneKey += () => IsActive = false;
                IsActive = false;
                _initialLocalPos = transform.localPosition;
            }
        }

        public void SetParentAndRestoreInitialPos(Transform parent)
        {
            transform.SetParent(parent);
            transform.localPosition = _initialLocalPos;
        }

        public void OnClick_KeyButton(KeyboardKeyPressResponse response)
        {
            if (ActiveInputField == null)
            {
                return;
            }

            switch (response.id)
            {
                case "space":
                    ActiveInputField.text += " ";
                    break;

                case "done":
                    OnClickDoneKey?.Invoke();
                    break;

                case "backspace":
                    if (ActiveInputField.text.Length > 0)
                    {
                        ActiveInputField.text = ActiveInputField.text.Substring(0, ActiveInputField.text.Length - 1);
                    }

                    break;

                default:
                    ActiveInputField.text += response.label
                        .Replace(".", NumberDecimalSeparator)
                        .Replace(",", NumberDecimalSeparator);
                    break;
            }
        }
    }

    public class KeyboardKeyPressResponse
    {
        public string id;
        public string label;
    }
}