﻿using System.Globalization;
using TMPro;
using UnityEngine;

namespace LetC
{
    [RequireComponent(typeof(KeyButton))]
    public class KeyButtonNumberDecimalSeparatorSetter : MonoBehaviour
    {
        private void Awake()
        {
            KeyButton keyButton = GetComponent<KeyButton>();
            if (keyButton.id == "." || keyButton.id == ",")
            {
                string numberDecimalSeparator = CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator;
                keyButton.id = numberDecimalSeparator;
                keyButton.text.GetComponent<TMP_Text>().SetText(numberDecimalSeparator);
            }
        }
    }
}