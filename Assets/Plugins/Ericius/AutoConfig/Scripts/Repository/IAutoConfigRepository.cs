﻿namespace Ericius.AutoConfig.Scripts.Repository
{
    public interface IAutoConfigRepository
    {
        int GetIntOrDefault(string key, int defaultValue);
        void SetInt(string key, int value);

        float GetFloatOrDefault(string key, float defaultValue);
        void SetFloat(string key, float value);

        bool GetBoolOrDefault(string key, bool defaultValue);
        void SetBool(string key, bool value);

        string GetStringOrDefault(string key, string defaultValue);
        void SetString(string key, string value);

        void Delete(string configKey);
    }
}