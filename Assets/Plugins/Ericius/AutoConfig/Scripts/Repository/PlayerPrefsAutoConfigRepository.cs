﻿using System;
using System.Globalization;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.Repository
{
    public class PlayerPrefsAutoConfigRepository : IAutoConfigRepository
    {
        public int GetIntOrDefault(string key, int defaultValue)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        public void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public float GetFloatOrDefault(string key, float defaultValue)
        {
            return PlayerPrefs.GetFloat(key, Convert.ToSingle(defaultValue, CultureInfo.InvariantCulture));
        }

        public void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        public void SetBool(string key, bool value)
        {
            PlayerPrefs.SetInt(key, value ? 1 : 0);
        }

        public string GetStringOrDefault(string key, string defaultValue)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }

        public void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        public bool GetBoolOrDefault(string key, bool defaultValue)
        {
            if (PlayerPrefs.HasKey(key))
            {
                return PlayerPrefs.GetInt(key) == 1;
            }

            return defaultValue;
        }

        public void Delete(string configKey)
        {
            PlayerPrefs.DeleteKey(configKey);
        }
    }
}