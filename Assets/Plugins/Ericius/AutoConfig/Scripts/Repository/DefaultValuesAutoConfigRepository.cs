﻿using System;
using System.Globalization;

namespace Ericius.AutoConfig.Scripts.Repository
{
    public class DefaultValuesAutoConfigRepository : IAutoConfigRepository
    {
        public int GetIntOrDefault(string key, int defaultValue)
        {
            return defaultValue;
        }

        public void SetInt(string key, int value)
        {
        }

        public float GetFloatOrDefault(string key, float defaultValue)
        {
            return Convert.ToSingle(defaultValue, CultureInfo.InvariantCulture);
        }

        public void SetFloat(string key, float value)
        {
        }

        public void SetBool(string key, bool value)
        {
        }

        public string GetStringOrDefault(string key, string defaultValue)
        {
            return defaultValue;
        }

        public void SetString(string key, string value)
        {
        }

        public bool GetBoolOrDefault(string key, bool defaultValue)
        {
            return defaultValue;
        }

        public void Delete(string configKey)
        {
        }
    }
}