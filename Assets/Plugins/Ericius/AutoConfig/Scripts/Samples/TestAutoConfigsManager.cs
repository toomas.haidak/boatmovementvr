﻿using System.Collections.Generic;
using Ericius.AutoConfig.Scripts.Core;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.Samples
{
    public class TestAutoConfigsManager : MonoBehaviour
    {
        private void Start()
        {
            ReadAndPrintConfigs();
            // UpdateConfigs();
        }

        private void UpdateConfigs()
        {
            List<AutoConfigEntryData> configs = AutoConfigManager.GetConfigs();

            configs[0].Value = 144;
            AutoConfigManager.SaveConfig(configs[0]);

            configs[3].Value = TestEnum.Value2;
            AutoConfigManager.SaveConfig(configs[3]);
        }

        private static void ReadAndPrintConfigs()
        {
            List<AutoConfigEntryData> configs = AutoConfigManager.GetConfigs();
            foreach (AutoConfigEntryData autoConfigEntryData in configs)
            {
                Debug.Log(autoConfigEntryData);
            }
        }
    }
}