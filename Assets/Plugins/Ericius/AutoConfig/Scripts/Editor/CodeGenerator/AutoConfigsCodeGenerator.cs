using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Ericius.AutoConfig.Scripts.Core;
using UnityEditor;

namespace Plugins.Ericius.AutoConfig.Scripts.Editor.CodeGenerator
{
    /// <summary>
    /// Utility to generate code that makes it easier to work with AutoConfigs.
    /// Based on UnityEngine.InputSystem.Editor.InputActionCodeGenerator.cs
    /// </summary>
    public static class AutoConfigsCodeGenerator
    {
        private const int KSpacesPerIndentLevel = 4;

        public struct Options
        {
            public string ClassName { get; set; }
            public string NamespaceName { get; set; }
            public string SourceAssetPath { get; set; }
        }

        public static string GenerateWrapperCode(AutoConfigSettings asset, Options options = default)
        {
            if (asset == null)
            {
                throw new ArgumentNullException(nameof(asset));
            }

            if (string.IsNullOrEmpty(options.SourceAssetPath))
            {
                options.SourceAssetPath = AssetDatabase.GetAssetPath(asset);
            }

            if (string.IsNullOrEmpty(options.ClassName) && !string.IsNullOrEmpty(asset.name))
            {
                options.ClassName = CSharpCodeHelpers.MakeTypeName(asset.name);
            }

            if (string.IsNullOrEmpty(options.ClassName))
            {
                if (string.IsNullOrEmpty(options.SourceAssetPath))
                {
                    throw new ArgumentException("options.sourceAssetPath");
                }

                options.ClassName = CSharpCodeHelpers.MakeTypeName(
                    Path.GetFileNameWithoutExtension(options.SourceAssetPath)
                );
            }

            Writer writer = new Writer
            {
                Buffer = new StringBuilder()
            };

            // Header.
            if (!string.IsNullOrEmpty(options.SourceAssetPath))
                writer.WriteLine($"// GENERATED AUTOMATICALLY FROM '{options.SourceAssetPath}'\n");

            // Usings.
            writer.WriteLine("using System;");
            writer.WriteLine("using System.Linq;");
            writer.WriteLine("using Ericius.AutoConfig.Scripts.Core;");
            writer.WriteLine("using UniRx;");
            writer.WriteLine("using UnityEngine;");
            writer.WriteLine("");

            // Begin namespace.
            var haveNamespace = !string.IsNullOrEmpty(options.NamespaceName);
            if (haveNamespace)
            {
                writer.WriteLine($"namespace {options.NamespaceName}");
                writer.BeginBlock();
            }

            // Begin class.
            writer.WriteLine($"public static partial class  {options.ClassName}");
            writer.BeginBlock();

            List<AutoConfigGroupSettings> autoConfigGroups = asset.Groups;
            for (int i = 0, iSize = autoConfigGroups.Count; i < iSize; i++)
            {
                GenerateGroupMethodsCode(writer, autoConfigGroups[i]);
            }

            GenerateHelperMethodsCode(writer);

            // End class.
            writer.EndBlock();

            // End namespace.
            if (haveNamespace)
                writer.EndBlock();

            return writer.Buffer.ToString();
        }

        private static void GenerateHelperMethodsCode(Writer writer)
        {
            writer.WriteIndent();
            writer.Write("//====================");
            writer.Write("HELPERS");
            writer.Write("====================");

            GenerateSubscribeToConfigChangesHelperMethodCode(writer);
            GenerateForceDefaultValuesSetter(writer);
        }

        private static void GenerateSubscribeToConfigChangesHelperMethodCode(Writer writer)
        {
            writer.WriteLine();
            writer.WriteLine(
                "/// <summary>Dispose self on target gameObject has been destroyed. Return value is self disposable.</summary>"
            );
            writer.WriteLine(
                "public static IDisposable SubscribeToConfigChanges(string configKey, Action onChanged, GameObject gameObject, bool fireNow)"
            );

            //method
            writer.BeginBlock();

            writer.WriteLine("void OnConfigChanged(AutoConfigEntryData configEntryData)");
            //local method
            writer.BeginBlock();

            writer.WriteLine("if (configEntryData.Key.EndsWith(configKey))");
            //if block
            writer.BeginBlock();
            writer.WriteLine("onChanged.Invoke();");
            //if block
            writer.EndBlock();

            //local method
            writer.EndBlock();

            writer.WriteLine();
            writer.WriteLine("if (fireNow)");
            writer.BeginBlock();
            writer.WriteLine("onChanged.Invoke();");
            writer.EndBlock();

            writer.WriteLine();
            writer.WriteLine(
                "return Observable.FromEvent(h => AutoConfigManager.OnChanged += OnConfigChanged,h => AutoConfigManager.OnChanged -= OnConfigChanged).Subscribe(_ => { }).AddTo(gameObject);"
            );

            //method
            writer.EndBlock();
        }

        private static void GenerateForceDefaultValuesSetter(Writer writer)
        {
            writer.WriteLine();
            writer.WriteLine(
                "public static void SetForceDefaultAutoConfigValues(bool force)"
            );

            //method
            writer.BeginBlock();

            writer.WriteLine("AutoConfigManager.SetForceDefaultAutoConfigValues(force);");

            //method
            writer.EndBlock();
        }

        private static void GenerateGroupMethodsCode(Writer writer, AutoConfigGroupSettings autoConfigGroup)
        {
            writer.WriteIndent();
            writer.Write("//====================");
            writer.Write(autoConfigGroup.Name);
            writer.Write("====================");
            writer.WriteLine();

            for (int i = 0, iSize = autoConfigGroup.Entries.Count; i < iSize; i++)
            {
                GenerateEntryMethodsCode(writer, autoConfigGroup.Entries[i]);
            }
        }

        private static void GenerateEntryMethodsCode(Writer writer, AutoConfigEntrySettings entry)
        {
            writer.WriteLine("//-------------------------------------------");
            string entryVariableName = ToVariableName(entry.Description);
            string entryKeyVariableName = $"{entryVariableName}Key";

            GenerateEntryGetter(writer, entry, entryKeyVariableName, entryVariableName);
            GenerateEntryValueChangeSubscribe(writer, entryKeyVariableName, entryVariableName);
        }

        private static void GenerateEntryGetter(
            Writer writer, AutoConfigEntrySettings entry, string entryKeyVariableName, string entryVariableName
        )
        {
            writer.WriteLine($"public const string {entryKeyVariableName} = \"{entry.Description}\";");
            writer.WriteLine();

            string dataType = GetDataType(entry);

            string getterMethodName = string.IsNullOrWhiteSpace(entry.GetterName)
                ? $"Get{entryVariableName}"
                : entry.GetterName;
            writer.WriteLine($"public static {dataType} {getterMethodName}()");
            writer.BeginBlock();

            writer.WriteLine(
                $"AutoConfigEntryData entryData = AutoConfigManager.GetConfigs().First(config => config.Key.EndsWith({entryKeyVariableName}));"
            );

            writer.WriteLine(
                $"return ({dataType}) AutoConfigManager.GetConfigValue(entryData);"
            );

            writer.EndBlock();
            writer.WriteLine();
        }

        private static string GetDataType(AutoConfigEntrySettings entry)
        {
            switch (entry.Type)
            {
                case AutoConfigType.Enum:
                    return entry.EnumType;

                default:
                    return entry.Type.ToString().ToLower();
            }
        }

        private static void GenerateEntryValueChangeSubscribe(
            Writer writer, string entryKeyVariableName, string entryVariableName
        )
        {
            writer.WriteLine(
                "/// <summary>Dispose self on target gameObject has been destroyed. Return value is self disposable.</summary>"
            );
            writer.WriteLine(
                $"public static IDisposable Subscribe{entryVariableName}Changed(Action onChanged, GameObject gameObject, bool fireNow = true)"
            );
            writer.BeginBlock();

            writer.WriteLine(
                $"return SubscribeToConfigChanges({entryKeyVariableName}, onChanged, gameObject, fireNow);"
            );

            writer.EndBlock();
            writer.WriteLine();
        }

        private static string ToVariableName(string value)
        {
            return value
                .Replace(" ", "")
                .Replace("(", "")
                .Replace(")", "");
        }

        // Updates the given file with wrapper code generated for the given action sets.
        private struct Writer
        {
            public StringBuilder Buffer;
            public int IndentLevel;

            public void BeginBlock()
            {
                WriteIndent();
                Buffer.Append("{\n");
                ++IndentLevel;
            }

            public void EndBlock()
            {
                --IndentLevel;
                WriteIndent();
                Buffer.Append("}\n");
            }

            public void WriteLine()
            {
                Buffer.Append('\n');
            }

            public void WriteLine(string text)
            {
                WriteIndent();
                Buffer.Append(text);
                Buffer.Append('\n');
            }

            public void Write(string text)
            {
                Buffer.Append(text);
            }

            public void WriteIndent()
            {
                for (var i = 0; i < IndentLevel; ++i)
                {
                    for (var n = 0; n < KSpacesPerIndentLevel; ++n)
                        Buffer.Append(' ');
                }
            }
        }

        // If the generated code is unchanged, does not touch the file.
        // Returns true if the file was touched, false otherwise.
        public static bool GenerateWrapperCode(string filePath, AutoConfigSettings asset, Options options)
        {
            if (!Path.HasExtension(filePath))
            {
                filePath += ".cs";
            }

            // Generate code.
            string code = GenerateWrapperCode(asset, options);

            // Check if the code changed. Don't write if it hasn't.
            if (File.Exists(filePath))
            {
                string existingCode = File.ReadAllText(filePath);
                if (existingCode == code ||
                    existingCode.WithAllWhitespaceStripped() == code.WithAllWhitespaceStripped())
                {
                    return false;
                }
            }

            // Write.
            EditorHelpers.CheckOut(filePath);
            File.WriteAllText(filePath, code);
            return true;
        }
    }
}