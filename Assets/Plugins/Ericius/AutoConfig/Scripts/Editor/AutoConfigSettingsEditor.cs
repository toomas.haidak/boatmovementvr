﻿using System.Collections.Generic;
using System.IO;
using Ericius.AutoConfig.Scripts.Core;
using Plugins.Ericius.AutoConfig.Scripts.Editor.CodeGenerator;
using Sirenix.OdinInspector.Editor;
using UnityEditor;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.Editor
{
    [CustomEditor(typeof(AutoConfigSettings))]
    public class AutoConfigSettingsEditor : OdinEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            this.HandleTargetsButton<AutoConfigSettings>(
                "Generate AutoConfig.cs", GenerateAutoConfig
            );
            this.HandleTargetsButton<AutoConfigSettings>(
                "Apply Current Values As Default", ApplyCurrentValuesAsDefault
            );
        }

        private void ApplyCurrentValuesAsDefault(AutoConfigSettings autoConfigSettings)
        {
            List<AutoConfigEntryData> entries = AutoConfigManager.GetConfigs();
            for (int i = 0, iSize = entries.Count; i < iSize; i++)
            {
                entries[i].Settings.DefaultValue = entries[i].Value.ToString();
            }
        }

        private void GenerateAutoConfig(AutoConfigSettings autoConfigSettings)
        {
            AutoConfigsCodeGenerator.Options options = new AutoConfigsCodeGenerator.Options
            {
                ClassName = autoConfigSettings.ClassName,
                NamespaceName = autoConfigSettings.Namespace
            };


            bool generated = AutoConfigsCodeGenerator.GenerateWrapperCode(
                Path.Combine(autoConfigSettings.Path, options.ClassName), autoConfigSettings, options
            );

            Debug.Log("Generate AutoConfig.cs: DONE");
            if (generated)
            {
                EditorUtility.RequestScriptReload();
            }
        }
    }
}