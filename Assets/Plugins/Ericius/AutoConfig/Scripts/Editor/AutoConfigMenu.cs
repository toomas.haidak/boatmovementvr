﻿using System.IO;
using Ericius.AutoConfig.Scripts.Core;
using UnityEditor;
using UnityEngine;

namespace Plugins.Ericius.AutoConfig.Scripts.Editor
{
    public static class AutoConfigMenu
    {
        [MenuItem("Tools/AutoConfigs/Reset", false, 20)]
        public static void ResetAutoConfigsMenu()
        {
            AutoConfigManager.ResetAutoConfigs();
        }

        [MenuItem("Tools/AutoConfigs/Create or Highlight Settings", false, 20)]
        public static void HighlightSettings()
        {
            Object targetSettingsAsset;

            AutoConfigSettings[] settingsArray = Resources.LoadAll<AutoConfigSettings>("");
            if (settingsArray.Length > 1)
            {
                Debug.LogWarning("Multiple AutoConfigSettings found. Highlighting first one...");
            }

            if (settingsArray.Length == 0)
            {
                targetSettingsAsset = CreateNewAutoConfigSettings();
            }
            else
            {
                targetSettingsAsset = settingsArray[0];
            }

            Selection.activeObject = targetSettingsAsset;
        }

        private static AutoConfigSettings CreateNewAutoConfigSettings()
        {
            const string directory = "Assets/Resources/";
            const string path = directory + "AutoConfigSettings.asset";
            Debug.Log("New AutoConfigSettings created at " + path);

            AutoConfigSettings asset = ScriptableObject.CreateInstance<AutoConfigSettings>();
            Directory.CreateDirectory(directory);
            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
            return AssetDatabase.LoadAssetAtPath<AutoConfigSettings>(path);
        }
    }
}