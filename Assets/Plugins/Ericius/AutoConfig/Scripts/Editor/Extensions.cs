﻿using System;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace Ericius.AutoConfig.Scripts.Editor
{
    public static class Extensions
    {
        public static void HandleTargets<T>(this UnityEditor.Editor editor, Action<T> handler) where T : Object
        {
            Object[] targets = editor.targets;

            for (int i = 0, iSize = targets.Length; i < iSize; i++)
            {
                T target = (T) targets[i];
                handler.Invoke(target);
                EditorUtility.SetDirty(target);
            }

            AssetDatabase.Refresh();
            AssetDatabase.SaveAssets();
        }

        public static void HandleTargetsButton<T>(this UnityEditor.Editor editor, string title, Action<T> handler) where T : Object
        {
            if (GUILayout.Button(title))
            {
                editor.HandleTargets(handler);
                Debug.Log($"'{title}' for {editor.targets.Length} targets: DONE");
            }
        }
    }
}