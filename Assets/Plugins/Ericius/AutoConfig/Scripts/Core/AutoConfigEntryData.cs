﻿namespace Ericius.AutoConfig.Scripts.Core
{
    public class AutoConfigEntryData
    {
        public readonly AutoConfigEntrySettings Settings;

        public readonly string Group;
        public readonly string Key;
        public object Value;

        public AutoConfigEntryData(AutoConfigEntrySettings settings,
            string group, string key, object value)
        {
            Settings = settings;
            Group = group;
            Key = key;
            Value = value;
        }

        public override string ToString()
        {
            return $"{Settings.Description}, Type: {Settings.Type}, Value: {Value}";
        }
    }
}