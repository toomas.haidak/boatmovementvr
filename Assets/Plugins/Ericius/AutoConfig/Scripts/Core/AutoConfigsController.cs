﻿using Ericius.AutoConfig.Scripts.UI;
using LetC;
using UnityEngine;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.Core
{
    public class AutoConfigsController : MonoBehaviour
    {
        public AutoConfigsWindowController AutoConfigsWindowController;
        public AutoConfigsModalPopup AutoConfigsModalPopup;
        public AutoConfigsModalWaitPopup AutoConfigsWaitPopup;

        [SerializeField] private Button ResetConfigsButton;
        [SerializeField] private Button CloseConfigsButton;

        private void Awake()
        {
            AddDefaultConfigTools();
            AutoConfigsWindowController.SetupAndShow();
        }

        private void OnEnable()
        {
            TextKeyboard textKeyboard = TextKeyboard.instance;
            if (textKeyboard != null)
            {
                textKeyboard.SetParentAndRestoreInitialPos(AutoConfigsWindowController.transform);
            }
        }

        private void AddDefaultConfigTools()
        {
            CloseConfigsButton.onClick.AddListener(() => { AutoConfigsWindowController.Hide(); });
            AutoConfigsWindowController.AddTool(CloseConfigsButton.transform, 100);

            ResetConfigsButton.onClick.AddListener(ResetAutoConfigs);
            AutoConfigsWindowController.AddTool(ResetConfigsButton.transform, 0);
        }

        private void ResetAutoConfigs()
        {
            AutoConfigsModalPopup.Show("Are you sure want to reset AutoConfigs to default values?", () =>
            {
                AutoConfigManager.ResetAutoConfigs();
                AutoConfigsWindowController.Refresh();
                AutoConfigsModalPopup.Show("Reset AutoConfigs: DONE", () => { }, null);
            }, () => { });
        }
    }
}