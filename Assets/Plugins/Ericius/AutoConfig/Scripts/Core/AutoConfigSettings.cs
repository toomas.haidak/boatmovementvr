﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.Core
{
    [CreateAssetMenu(fileName = "AutoConfigSettings", menuName = "AutoConfigSettings")]
    public partial class AutoConfigSettings : ScriptableObject
    {
        [SerializeField] public bool ForceDefaultValues;

        [ListDrawerSettings(ListElementLabelName = "Name", ShowIndexLabels = false)]
        public List<AutoConfigGroupSettings> Groups = new List<AutoConfigGroupSettings>();
    }

    [Serializable]
    public class AutoConfigGroupSettings
    {
        public string Name = "COMMON";

        [ListDrawerSettings(ListElementLabelName = "Description", ShowIndexLabels = false)]
        public List<AutoConfigEntrySettings> Entries = new List<AutoConfigEntrySettings>();
    }

    [Serializable]
    public class AutoConfigEntrySettings
    {
        public string Description;

        public AutoConfigType Type;
        public string EnumType;

        public string DefaultValue;

        public string GetterName;
    }
}