﻿using System;
using System.Collections.Generic;
using Ericius.AutoConfig.Scripts.Repository;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.Core
{
    public static class AutoConfigManager
    {
        private const string ConfigKeyPrefix = "ericius.autoconfig.";

        private static bool _initialized;
        private static AutoConfigSettings _settings;

        private static readonly List<AutoConfigEntryData> ConfigEntries = new List<AutoConfigEntryData>();

        private static readonly IAutoConfigRepository Repository = new PlayerPrefsAutoConfigRepository();
        private static readonly IAutoConfigRepository DefaultValuesRepository = new DefaultValuesAutoConfigRepository();

        public static event Action<AutoConfigEntryData> OnChanged;

        public static List<AutoConfigEntryData> GetConfigs()
        {
            TryInit();
            return ConfigEntries;
        }

        public static object GetConfigValue(AutoConfigEntryData entryData)
        {
            TryInit();
            return GetConfigValue(entryData.Key, entryData.Settings);
        }

        public static void SaveConfig(AutoConfigEntryData autoConfigEntryData)
        {
            TryInit();
            UpdateCacheValue(autoConfigEntryData);

            switch (autoConfigEntryData.Settings.Type)
            {
                case AutoConfigType.Int:
                    Repository.SetInt(
                        autoConfigEntryData.Key, AutoConfigTypeHelper.ToInt(autoConfigEntryData.Value)
                    );
                    break;

                case AutoConfigType.Float:
                    Repository.SetFloat(
                        autoConfigEntryData.Key, AutoConfigTypeHelper.ToFloat(autoConfigEntryData.Value)
                    );
                    break;

                case AutoConfigType.Bool:
                    Repository.SetBool(
                        autoConfigEntryData.Key, AutoConfigTypeHelper.ToBool(autoConfigEntryData.Value)
                    );
                    break;

                case AutoConfigType.String:
                    Repository.SetString(
                        autoConfigEntryData.Key, AutoConfigTypeHelper.ToString(autoConfigEntryData.Value)
                    );
                    break;

                case AutoConfigType.Enum:
                    Repository.SetInt(
                        autoConfigEntryData.Key, AutoConfigTypeHelper.ToInt(autoConfigEntryData.Value)
                    );
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }

            FireChanged(autoConfigEntryData);
        }

        private static void TryInit()
        {
            if (!_initialized)
            {
                if (!InitSettings()) return;
                InitConfigEntries();
                _initialized = true;
            }
        }

        private static bool InitSettings()
        {
            AutoConfigSettings[] settingsArray = Resources.LoadAll<AutoConfigSettings>("");
            if (settingsArray.Length == 0)
            {
                Debug.LogWarning("AutoConfigSettings not found. Create one with context menu 'AutoConfigSettings'");
                return false;
            }

            if (settingsArray.Length > 1)
            {
                Debug.LogWarning("Multiple AutoConfigSettings found. First one will be selected!");
            }

            _settings = settingsArray[0];
            return true;
        }

        private static void InitConfigEntries()
        {
            if (_settings != null) InitConfigEntries(_settings);
        }

        private static void InitConfigEntries(AutoConfigSettings settings)
        {
            ConfigEntries.Clear();
            for (int i = 0; i < settings.Groups.Count; i++)
            {
                AutoConfigGroupSettings groupSettings = settings.Groups[i];
                foreach (AutoConfigEntrySettings entrySettings in groupSettings.Entries)
                {
                    ConfigEntries.Add(GetConfigEntry(groupSettings, entrySettings));
                }
            }
        }

        public static AutoConfigEntryData GetConfigEntry(AutoConfigGroupSettings cGroup,
            AutoConfigEntrySettings cEntry)
        {
            string configKey = GetConfigKey(cGroup.Name, cEntry.Description);
            return new AutoConfigEntryData(
                cEntry, cGroup.Name, configKey, GetConfigValue(configKey, cEntry)
            );
        }

        private static object GetConfigValue(string configKey, AutoConfigEntrySettings cEntry)
        {
            IAutoConfigRepository repository = _settings.ForceDefaultValues ? DefaultValuesRepository : Repository;

            switch (cEntry.Type)
            {
                case AutoConfigType.Int:
                    int defaultIntValue = AutoConfigTypeHelper.ToInt(cEntry.DefaultValue);
                    return repository.GetIntOrDefault(configKey, defaultIntValue);

                case AutoConfigType.Float:
                    return repository.GetFloatOrDefault(
                        configKey, AutoConfigTypeHelper.ToFloat(cEntry.DefaultValue)
                    );

                case AutoConfigType.Bool:
                    return repository.GetBoolOrDefault(
                        configKey, AutoConfigTypeHelper.ToBool(cEntry.DefaultValue)
                    );

                case AutoConfigType.String:
                    return repository.GetStringOrDefault(
                        configKey, cEntry.DefaultValue
                    );

                case AutoConfigType.Enum:
                    Type enumType = AutoConfigTypeHelper.GetEnumType(cEntry.EnumType);
                    if (enumType == null)
                    {
                        Debug.LogWarning($"Enum type '{cEntry.EnumType}' not found. Return zero value");
                        return 0;
                    }

                    Enum defaultEnumValue = AutoConfigTypeHelper.ToEnum(enumType, cEntry.DefaultValue);
                    return repository.GetIntOrDefault(
                        configKey, AutoConfigTypeHelper.ToInt(defaultEnumValue)
                    );

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static string GetConfigKey(string entryGroup, string entryKey)
        {
            return ConfigKeyPrefix + entryGroup + "." + entryKey;
        }

        private static void UpdateCacheValue(AutoConfigEntryData configEntryData)
        {
            List<AutoConfigEntryData> configs = GetConfigs();

            foreach (AutoConfigEntryData attribute in configs)
            {
                if (attribute.Key.Equals(configEntryData.Key))
                {
                    attribute.Value = configEntryData.Value;
                    return;
                }
            }
        }

        public static void ApplyConfig(string configKey, string configStrValue)
        {
            List<AutoConfigEntryData> configs = GetConfigs();
            foreach (AutoConfigEntryData config in configs)
            {
                if (config.Key.Equals(configKey))
                {
                    ApplyConfigValueFromString(config, configStrValue);
                    SaveConfig(config);
                    return;
                }
            }

            Debug.LogWarningFormat("Config '{0}' not found", configKey);
        }

        public static void ApplyConfigValueFromString(AutoConfigEntryData config, string configStrValue)
        {
            switch (config.Settings.Type)
            {
                case AutoConfigType.Int:
                    config.Value = AutoConfigTypeHelper.ToInt(configStrValue);
                    break;

                case AutoConfigType.Float:
                    config.Value = AutoConfigTypeHelper.ToFloat(configStrValue);
                    break;

                case AutoConfigType.Bool:
                    config.Value = AutoConfigTypeHelper.ToBool(configStrValue);
                    break;

                case AutoConfigType.String:
                    config.Value = configStrValue;
                    break;

                case AutoConfigType.Enum:
                    Type enumType = AutoConfigTypeHelper.GetEnumType(config.Settings.EnumType);
                    if (enumType != null)
                    {
                        config.Value = AutoConfigTypeHelper.ToEnum(enumType, configStrValue);
                    }
                    else
                    {
                        Debug.LogWarning(
                            $"Enum type '{config.Settings.EnumType}' not found. Config value not applied."
                        );
                    }

                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private static void FireChanged(AutoConfigEntryData config)
        {
            OnChanged?.Invoke(config);
        }


        private static void FireChanged(IReadOnlyList<AutoConfigEntryData> configEntries)
        {
            for (int i = 0, iSize = configEntries.Count; i < iSize; i++)
            {
                FireChanged(configEntries[i]);
            }
        }

        public static void ResetAutoConfigs()
        {
            TryInit();

            List<AutoConfigEntryData> configs = GetConfigs();
            foreach (AutoConfigEntryData config in configs)
            {
                Repository.Delete(config.Key);
            }

            ResetCache();
            FireChanged(ConfigEntries);
            Debug.Log("Reset Auto Configs: DONE");
        }

        private static void ResetCache()
        {
            InitConfigEntries();
        }

        public static void SetForceDefaultAutoConfigValues(bool force)
        {
            TryInit();
            _settings.ForceDefaultValues = force;
        }
    }
}