﻿#if UNITY_EDITOR
using Sirenix.OdinInspector;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.Core
{
    public partial class AutoConfigSettings
    {
        [Title("Class Generator")] [SerializeField]
        public string ClassName = "AutoConfigs";
        public string Namespace = "Ericius";

        [SerializeField] public string Path = "Assets/";
    }
}

#endif