﻿namespace Ericius.AutoConfig.Scripts.Core
{
    public enum AutoConfigType
    {
        Int,
        Float,
        Bool,
        String,
        Enum
    }
}