﻿using System;
using System.Collections;
using System.Globalization;
using System.Reflection;

namespace Ericius.AutoConfig.Scripts.Core
{
    public static class AutoConfigTypeHelper
    {
        public static int ToInt(string value)
        {
            return Convert.ToInt32(value, CultureInfo.InvariantCulture);
        }

        public static int ToInt(object value)
        {
            return Convert.ToInt32(value, CultureInfo.InvariantCulture);
        }

        public static float ToFloat(string value)
        {
            return Convert.ToSingle(value.Replace(",", "."), CultureInfo.InvariantCulture);
        }

        public static float ToFloat(object value)
        {
            return Convert.ToSingle(value, CultureInfo.InvariantCulture);
        }

        public static bool ToBool(string value)
        {
            return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
        }

        public static bool ToBool(object value)
        {
            return Convert.ToBoolean(value, CultureInfo.InvariantCulture);
        }

        public static string ToString(object value)
        {
            return value.ToString();
        }

        public static Enum ToEnum(Type enumType, string value)
        {
            return (Enum) Enum.Parse(enumType, value);
        }

        // public static Enum ToEnum(Type enumType, object value)
        // {
        //     return (Enum) Enum.Parse(enumType, value);
        // }

        public static Type GetEnumType(string enumName)
        {
            foreach (Assembly assembly in AppDomain.CurrentDomain.GetAssemblies())
            {
                Type type = assembly.GetType(enumName);
                if (type == null)
                    continue;
                if (type.IsEnum)
                    return type;
            }

            return null;
        }

        public static IEnumerable GetEnumValues(string enumName)
        {
            return Enum.GetValues(GetEnumType(enumName));
        }
    }
}