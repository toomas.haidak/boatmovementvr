﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI
{
    public class AutoConfigsModalPopup : MonoBehaviour
    {
        [SerializeField] private Text InfoText;
        [SerializeField] private Button ConfirmButton;
        [SerializeField] private Button CancelButton;

        public void Show(string infoText, UnityAction onConfirm, UnityAction onCancel)
        {
            InfoText.text = infoText;
            SetupButton(ConfirmButton, onConfirm);
            SetupButton(CancelButton, onCancel);

            gameObject.SetActive(true);
        }

        private void SetupButton(Button button, UnityAction action)
        {
            if (action != null)
            {
                button.onClick.RemoveAllListeners();
                button.onClick.AddListener(() =>
                {
                    gameObject.SetActive(false);
                    action.Invoke();
                });
                button.gameObject.SetActive(true);
            }
            else
            {
                button.gameObject.SetActive(false);
            }
        }
    }
}