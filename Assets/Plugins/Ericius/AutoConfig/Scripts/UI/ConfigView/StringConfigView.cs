﻿using System;
using Ericius.AutoConfig.Scripts.Core;
using LetC;
using TMPro;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI.ConfigView
{
    public class StringConfigView : AbstractConfigView
    {
        public Text Caption;
        public TMP_InputField Value;

        public override void SetConfig(AutoConfigEntryData configEntry)
        {
            Caption.text = configEntry.Settings.Description;
            Value.text = configEntry.Value.ToString();

            Value.onValueChanged.AddListener(value =>
            {
                configEntry.Value = GetValue();
                AutoConfigManager.SaveConfig(configEntry);
            });

            SetInputFieldPositionManager();
        }

        private void SetInputFieldPositionManager()
        {
            Value.onSelect.AddListener(_ =>
            {
                TextKeyboard textKeyboard = TextKeyboard.instance;
                if (textKeyboard != null)
                {
                    textKeyboard.IsActive = true;
                    textKeyboard.ActiveInputField = Value;
                    Value.caretPosition = int.MaxValue;
                }
            });
        }

        public override object GetValue()
        {
            return Value.text;
        }

        public override AutoConfigType GetDataType()
        {
            return AutoConfigType.String;
        }
    }
}