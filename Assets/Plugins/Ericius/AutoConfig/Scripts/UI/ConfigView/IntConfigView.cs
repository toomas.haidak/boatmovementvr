﻿using System;
using Ericius.AutoConfig.Scripts.Core;

namespace Ericius.AutoConfig.Scripts.UI.ConfigView
{
    public class IntConfigView : StringConfigView
    {
        public override object GetValue()
        {
            return Convert.ToInt32(Value.text);
        }

        public override AutoConfigType GetDataType()
        {
            return AutoConfigType.Int;
        }
    }
}