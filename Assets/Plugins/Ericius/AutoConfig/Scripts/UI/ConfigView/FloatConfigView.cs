﻿using Ericius.AutoConfig.Scripts.Core;

namespace Ericius.AutoConfig.Scripts.UI.ConfigView
{
    public class FloatConfigView : StringConfigView
    {
        public override object GetValue()
        {
            return float.Parse(Value.text);
        }

        public override AutoConfigType GetDataType()
        {
            return AutoConfigType.Float;
        }
    }
}