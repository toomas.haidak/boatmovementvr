﻿using Ericius.AutoConfig.Scripts.Core;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI.ConfigView
{
    public class BoolConfigView : AbstractConfigView
    {
        public Text Caption;
        public Toggle Value;

        public override void SetConfig(AutoConfigEntryData configEntry)
        {
            name = configEntry.Settings.Description;
            Caption.text = configEntry.Settings.Description;
            Value.isOn = (bool) configEntry.Value;

            Value.onValueChanged.AddListener(value =>
            {
                configEntry.Value = GetValue();
                AutoConfigManager.SaveConfig(configEntry);
            });
        }

        public override object GetValue()
        {
            return Value.isOn;
        }

        public override AutoConfigType GetDataType()
        {
            return AutoConfigType.Bool;
        }
    }
}