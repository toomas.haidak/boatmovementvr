﻿using Ericius.AutoConfig.Scripts.Core;
using UnityEngine;

namespace Ericius.AutoConfig.Scripts.UI.ConfigView
{
    public abstract class AbstractConfigView : MonoBehaviour
    {
        public abstract void SetConfig(AutoConfigEntryData configEntry);
        public abstract object GetValue();
        public abstract AutoConfigType GetDataType();
    }
}