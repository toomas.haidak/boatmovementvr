﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ericius.AutoConfig.Scripts.Core;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI.ConfigView
{
    public class EnumConfigView : AbstractConfigView
    {
        public Text Caption;
        public BoolConfigView BoolConfigViewPrefab;

        private readonly List<BoolConfigView> _enumValueViews = new List<BoolConfigView>();
        private BoolConfigView _currentActiveToggle;

        public override void SetConfig(AutoConfigEntryData configEntry)
        {
            Caption.text = configEntry.Settings.Description;

            int activeOptionValue = AutoConfigTypeHelper.ToInt(configEntry.Value);

            Dictionary<int, BoolConfigView> enumValueToBoolConfigView = new Dictionary<int, BoolConfigView>();
            foreach (object enumValue in AutoConfigTypeHelper.GetEnumValues(configEntry.Settings.EnumType))
            {
                int intEnumValue = (int) enumValue;

                BoolConfigView boolConfigView = Instantiate(BoolConfigViewPrefab, transform);
                boolConfigView.name = enumValue.ToString();
                bool isActiveToggle = intEnumValue == activeOptionValue;
                if (isActiveToggle)
                {
                    _currentActiveToggle = boolConfigView;
                    boolConfigView.Value.isOn = true;
                }
                else
                {
                    boolConfigView.Value.isOn = false;
                }

                boolConfigView.Caption.text = enumValue.ToString();

                _enumValueViews.Add(boolConfigView);

                enumValueToBoolConfigView[intEnumValue] = boolConfigView;
            }

            foreach (KeyValuePair<int, BoolConfigView> keyValuePair in enumValueToBoolConfigView)
            {
                int intEnumValue = keyValuePair.Key;
                BoolConfigView boolConfigView = keyValuePair.Value;

                boolConfigView.Value.onValueChanged.AddListener(newValue =>
                {
                    if (newValue == true)
                    {
                        OnNewEnumValueSelected(intEnumValue, boolConfigView, configEntry);
                    }
                    else
                    {
                        if (!AnyTogglesOn())
                        {
                            _currentActiveToggle.Value.isOn = true; //reactivate last active toggle
                        }
                    }
                });
            }
        }

        private bool AnyTogglesOn()
        {
            return _enumValueViews.Any(boolConfigView => boolConfigView.Value.isOn);
        }

        private void OnNewEnumValueSelected(int intEnumValue, BoolConfigView boolConfigView,
            AutoConfigEntryData configEntry)
        {
            _currentActiveToggle = boolConfigView;
            foreach (BoolConfigView enumValueView in _enumValueViews)
            {
                if (enumValueView == boolConfigView)
                {
                    configEntry.Value = intEnumValue;
                    AutoConfigManager.SaveConfig(configEntry);
                }
                else
                {
                    enumValueView.Value.isOn = false;
                }
            }
        }

        public override object GetValue()
        {
            throw new NotImplementedException();
        }

        public override AutoConfigType GetDataType()
        {
            return AutoConfigType.Enum;
        }
    }
}