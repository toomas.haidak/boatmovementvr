﻿using UnityEngine;

namespace Ericius.AutoConfig.Scripts.UI
{
    public class AutoConfigsModalWaitPopup : MonoBehaviour
    {
        [SerializeField] private Transform WaitIcon;
        [SerializeField] private float RotationSpeed = -100f;

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        private void Update()
        {
            WaitIcon.Rotate(Vector3.forward, Time.deltaTime * RotationSpeed);
        }
    }
}