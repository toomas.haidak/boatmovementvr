﻿using System;
using System.Collections;
using System.Collections.Generic;
using Ericius.AutoConfig.Scripts.Core;
using Ericius.AutoConfig.Scripts.UI.Accordion;
using Ericius.AutoConfig.Scripts.UI.ConfigView;
using UnityEngine;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI
{
    public class AutoConfigsWindowController : MonoBehaviour
    {
        private const string LastWorkGroupKey = "Ericius.AutoConfig.LastWorkGroup";
        private const string LastScrollRectPosKey = "Ericius.AutoConfig.LastScrollRectPos";

        [SerializeField] private Transform ConfigsContainer;
        [SerializeField] private UiAccordion UiAccordion;
        [SerializeField] private Transform ToolsPanel;
        [SerializeField] private ScrollRect ScrollRect;

        [SerializeField] private AbstractConfigView[] ConfigViewPrefabs;

        public void SetupAndShow()
        {
            ClearConfigViews();

            List<AutoConfigEntryData> configs = AutoConfigManager.GetConfigs();
            foreach (AutoConfigEntryData configEntry in configs)
            {
                AddConfigView(configEntry);
            }

            gameObject.SetActive(true);
            SetupAndShowWorkGroup();
        }

        private void SetupAndShowWorkGroup()
        {
            UiAccordion.AddSlotExpandListener(slot => { PlayerPrefs.SetString(LastWorkGroupKey, slot.SlotId); });

            if (PlayerPrefs.HasKey(LastWorkGroupKey))
            {
                if (!UiAccordion.TryExpandSlotById(PlayerPrefs.GetString(LastWorkGroupKey)))
                {
                    UiAccordion.ExpandFirstSlot();
                }
            }
            else
            {
                UiAccordion.ExpandFirstSlot();
            }

            StartCoroutine(TryRestoreScrollPos());
        }

        private IEnumerator TryRestoreScrollPos()
        {
            yield return null;
            if (PlayerPrefs.HasKey(LastScrollRectPosKey))
            {
                ScrollRect.verticalNormalizedPosition = PlayerPrefs.GetFloat(LastScrollRectPosKey);
            }
        }

        public void Refresh()
        {
            PlayerPrefs.SetFloat(LastScrollRectPosKey, ScrollRect.verticalNormalizedPosition);
            SetupAndShow();
        }

        private void ClearConfigViews()
        {
            UiAccordion.Clear();
        }

        private void AddConfigView(AutoConfigEntryData configEntry)
        {
            AbstractConfigView configViewPrefab = FindConfigViewByType(configEntry.Settings.Type);

            if (configViewPrefab == null)
            {
                throw new ArgumentOutOfRangeException();
            }

            AbstractConfigView configView = Instantiate(configViewPrefab, ConfigsContainer);
            configView.SetConfig(configEntry);

            UiAccordion.AddSlotContent(configEntry.Group, configView.transform);
        }

        private AbstractConfigView FindConfigViewByType(AutoConfigType dataType)
        {
            foreach (AbstractConfigView abstractConfigView in ConfigViewPrefabs)
            {
                AutoConfigType configViewDataType = abstractConfigView.GetDataType();
                if (configViewDataType == dataType)
                {
                    return abstractConfigView;
                }
            }

            return null;
        }

        public void Hide()
        {
            PlayerPrefs.SetFloat(LastScrollRectPosKey, ScrollRect.verticalNormalizedPosition);

            gameObject.SetActive(false);
            ClearConfigViews();
        }


        public void AddTool(Transform tool, int siblingIndex = -1)
        {
            tool.SetParent(ToolsPanel);
            if (siblingIndex >= 0) tool.SetSiblingIndex(siblingIndex);

            tool.localScale = Vector3.one;
            Vector3 toolLocalPosition = tool.localPosition;
            toolLocalPosition.z = 0f;
            tool.localPosition = toolLocalPosition;

            tool.localRotation = Quaternion.identity;
        }
    }
}