﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI.Accordion
{
    public class UiAccordionSlot : MonoBehaviour
    {
        [SerializeField] private Text Caption;
        [SerializeField] private Transform DataContainer;
        [SerializeField] private float MinSlotHeight = 50f;
        [SerializeField] private float CollapseTime = 0.2f;
        [SerializeField] private float ExpandTime = 0.2f;
        [SerializeField] private Transform[] CollapseIcons;

        private readonly List<Transform> _dataContent = new List<Transform>();

        private RectTransform _rectTransform;
        private Action<UiAccordionSlot> _onClickAction;
        private Action<UiAccordionSlot> _onExpandAction;
        private Action _onChangeSizeAction;

        private readonly Vector3 _expandedScale = new Vector3(1f, 1f, 1f);
        private readonly Vector3 _collapsedScale = new Vector3(1f, 0f, 1f);

        private float _changeSizeTimer;
        private float _changeSizeTime;
        private Vector3 _fromScale;
        private Vector3 _toScale;
        private bool _changeSizeDelta;
        private Vector2 _fromSizeDelta;
        private Vector2 _toSizeDelta;

        private float _fromCollapseIconRotation;
        private float _toCollapseIconRotation;

        public string SlotId { get; private set; }
        public bool Collapsed { get; private set; }

        public void Setup(string slotId, Action<UiAccordionSlot> onClickAction, Action<UiAccordionSlot> onExpandAction,
            Action onChangeSizeAction = null)
        {
            SlotId = slotId;
            _onClickAction = onClickAction;
            _onExpandAction = onExpandAction;
            _onChangeSizeAction = onChangeSizeAction;
            _rectTransform = GetComponent<RectTransform>();
            Caption.text = slotId;
        }

        public void Expand()
        {
            Collapsed = false;

            _changeSizeDelta = false;
            _changeSizeTime = ExpandTime;
            _changeSizeTimer = _changeSizeTime;
            _fromScale = _dataContent[0].localScale;
            _toScale = _expandedScale;

            _fromCollapseIconRotation = CollapseIcons[0].localRotation.eulerAngles.z;
            _toCollapseIconRotation = 180f;

            _onExpandAction.Invoke(this);
        }

        public void Collapse()
        {
            Collapsed = true;

            _changeSizeDelta = true;

            Vector2 sizeDelta = _rectTransform.sizeDelta;
            _fromSizeDelta = sizeDelta;
            sizeDelta.y = MinSlotHeight;
            _toSizeDelta = sizeDelta;

            _changeSizeTime = CollapseTime;
            _changeSizeTimer = _changeSizeTime;
            _fromScale = _dataContent[0].localScale;
            _toScale = _collapsedScale;

            _fromCollapseIconRotation = CollapseIcons[0].rotation.eulerAngles.z;
            _toCollapseIconRotation = 0f;
        }

        public void OnClick()
        {
            _onClickAction(this);
        }

        public void AddContent(Transform content)
        {
            content.SetParent(DataContainer);
            _dataContent.Add(content);
        }

        private void SetCollapseIconsAngle(float angle)
        {
            for (int i = 0; i < CollapseIcons.Length; i++)
            {
                CollapseIcons[i].transform.localRotation = Quaternion.Euler(0f, 0f, angle);
            }
        }

        private void Update()
        {
            if (_changeSizeTimer > 0f)
            {
                _changeSizeTimer -= Time.deltaTime;
                _changeSizeTimer = Mathf.Max(0f, _changeSizeTimer);
                float changeSizeProgress01 = EaseInOutSine(1f - _changeSizeTimer / _changeSizeTime);

                int i;
                for (i = 0; i < _dataContent.Count; i++)
                {
                    _dataContent[i].localScale = Vector3.Lerp(
                        _fromScale, _toScale, changeSizeProgress01
                    );
                }

                for (i = 0; i < CollapseIcons.Length; i++)
                {
                    SetCollapseIconsAngle(
                        Mathf.Lerp(_fromCollapseIconRotation, _toCollapseIconRotation, changeSizeProgress01)
                    );
                }

                if (_changeSizeDelta)
                {
                    _rectTransform.sizeDelta = Vector2.Lerp(_fromSizeDelta, _toSizeDelta, changeSizeProgress01);
                }

                _onChangeSizeAction?.Invoke();
            }
        }

        private static float EaseInOutSine(float value)
        {
            return -(Mathf.Cos(Mathf.PI * value) - 1) / 2;
        }
    }
}