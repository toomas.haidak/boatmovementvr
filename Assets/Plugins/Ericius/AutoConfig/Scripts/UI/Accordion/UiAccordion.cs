﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Ericius.AutoConfig.Scripts.UI.Accordion
{
    public class UiAccordion : MonoBehaviour
    {
        [SerializeField] private UiAccordionSlot SlotPrefab;
        [SerializeField] private RectTransform SlotsContainer;

        private readonly List<UiAccordionSlot> _slots = new List<UiAccordionSlot>();
        private readonly List<Action<UiAccordionSlot>> _onSlotExpandListeners = new List<Action<UiAccordionSlot>>();

        public void AddSlotContent(string slotId, Transform content)
        {
            UiAccordionSlot accordionSlot = _slots.FirstOrDefault((slot) => slot.SlotId.Equals(slotId));
            if (accordionSlot == null)
            {
                accordionSlot = CreateSlot(slotId);
            }

            accordionSlot.AddContent(content);
        }

        public void AddSlotExpandListener(Action<UiAccordionSlot> onExpand)
        {
            _onSlotExpandListeners.Add(onExpand);
        }

        public void ClearSlotExpandListeners()
        {
            _onSlotExpandListeners.Clear();
        }

        private UiAccordionSlot CreateSlot(string slotId)
        {
            UiAccordionSlot newSlot = Instantiate(SlotPrefab, SlotsContainer.transform);
            newSlot.Setup(slotId, OnSlotClick, OnSlotExpand, OnSlotChangeSize);

            _slots.Add(newSlot);
            return newSlot;
        }

        private void OnSlotChangeSize()
        {
            ForceUpdateLayout(SlotsContainer);
        }
        
        private void OnSlotExpand(UiAccordionSlot slot)
        {
            for (int i = 0; i < _onSlotExpandListeners.Count; i++)
            {
                _onSlotExpandListeners[i].Invoke(slot);
            }
        }

        private void OnSlotClick(UiAccordionSlot slot)
        {
            if (slot.Collapsed)
            {
                slot.Expand();

                for (int i = 0; i < _slots.Count; i++)
                {
                    UiAccordionSlot accordionSlot = _slots[i];

                    if (accordionSlot.Equals(slot)) continue;
                    if (accordionSlot.Collapsed) continue;

                    accordionSlot.Collapse();
                }
            }
            else
            {
                slot.Collapse();
            }

            ForceUpdateLayout(SlotsContainer);
        }

        public void ExpandFirstSlot()
        {
            if (_slots.Any())
            {
                ExpandSlot(_slots[0]);
            }
        }

        public bool TryExpandSlotById(string slotId)
        {
            UiAccordionSlot uiAccordionSlot = _slots.FirstOrDefault(slot => slot.SlotId.Equals(slotId));
            if (uiAccordionSlot == null) return false;

            ExpandSlot(uiAccordionSlot);
            return true;
        }

        public void ExpandSlot(UiAccordionSlot slot)
        {
            for (int i = 0; i < _slots.Count; i++)
            {
                UiAccordionSlot uiAccordionSlot = _slots[i];
                if (slot == uiAccordionSlot)
                {
                    uiAccordionSlot.Expand();
                }
                else
                {
                    uiAccordionSlot.Collapse();
                }
            }
        }

        private void ForceUpdateLayouts(RectTransform[] layouts)
        {
            for (int i = 0; i < layouts.Length; i++)
            {
                ForceUpdateLayout(layouts[i]);
            }
        }

        private void ForceUpdateLayout(RectTransform layout)
        {
            if (layout != null && layout.gameObject.activeInHierarchy)
            {
                StartCoroutine(UpdateLayout(layout));
            }
        }

        private IEnumerator UpdateLayout(RectTransform layout)
        {
            yield return null;
            LayoutRebuilder.ForceRebuildLayoutImmediate(layout);
        }

        public void Clear()
        {
            for (int i = 0; i < _slots.Count; i++)
            {
                Destroy(_slots[i].gameObject);
            }

            _slots.Clear();
            ClearSlotExpandListeners();
        }
    }
}