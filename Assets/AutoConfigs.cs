// GENERATED AUTOMATICALLY FROM 'Assets/Resources/AutoConfigSettings.asset'

using System;
using System.Linq;
using Ericius.AutoConfig.Scripts.Core;
using UniRx;
using UnityEngine;

namespace Ericius
{
    public static partial class  AutoConfigs
    {
        //====================Boat====================
        //-------------------------------------------
        public const string SpeedKey = "Speed";

        public static float GetSpeed()
        {
            AutoConfigEntryData entryData = AutoConfigManager.GetConfigs().First(config => config.Key.EndsWith(SpeedKey));
            return (float) AutoConfigManager.GetConfigValue(entryData);
        }

        /// <summary>Dispose self on target gameObject has been destroyed. Return value is self disposable.</summary>
        public static IDisposable SubscribeSpeedChanged(Action onChanged, GameObject gameObject, bool fireNow = true)
        {
            return SubscribeToConfigChanges(SpeedKey, onChanged, gameObject, fireNow);
        }

        //-------------------------------------------
        public const string TurboKey = "Turbo";

        public static float GetTurbo()
        {
            AutoConfigEntryData entryData = AutoConfigManager.GetConfigs().First(config => config.Key.EndsWith(TurboKey));
            return (float) AutoConfigManager.GetConfigValue(entryData);
        }

        /// <summary>Dispose self on target gameObject has been destroyed. Return value is self disposable.</summary>
        public static IDisposable SubscribeTurboChanged(Action onChanged, GameObject gameObject, bool fireNow = true)
        {
            return SubscribeToConfigChanges(TurboKey, onChanged, gameObject, fireNow);
        }

        //====================HELPERS====================
        /// <summary>Dispose self on target gameObject has been destroyed. Return value is self disposable.</summary>
        public static IDisposable SubscribeToConfigChanges(string configKey, Action onChanged, GameObject gameObject, bool fireNow)
        {
            void OnConfigChanged(AutoConfigEntryData configEntryData)
            {
                if (configEntryData.Key.EndsWith(configKey))
                {
                    onChanged.Invoke();
                }
            }

            if (fireNow)
            {
                onChanged.Invoke();
            }

            return Observable.FromEvent(h => AutoConfigManager.OnChanged += OnConfigChanged,h => AutoConfigManager.OnChanged -= OnConfigChanged).Subscribe(_ => { }).AddTo(gameObject);
        }

        public static void SetForceDefaultAutoConfigValues(bool force)
        {
            AutoConfigManager.SetForceDefaultAutoConfigValues(force);
        }
    }
}
